## Jak přispět? ##

----

Díky za zájem. Přispívat do projektu je možné spoustou různých způsobů. Pokusil jsem se tady navrhnout tři, podle toho, kolik času se vám chce překladu věnovat, a podle vaší technické zdatnosti.

Překlad v celém projektu využívá formátování Markdown, které je naštěstí velice jednoduché. V nápovědě GitLabu se můžete seznámit se [základy formátu](https://about.gitlab.com/handbook/marketing/developer-relations/technical-writing/markdown-guide/), například: *kurzíva* (`*hvězdičkami*`), **tučný text** (`**dvěma hvězdičkami**`), nadpis (`# jeden křížek pro h1 #`, `## dva pro h2 ##` atd.), blok citace (`> odstavec citace`), [hypertextový odkaz](www.example.com) (`[Můj buřičský web](www.example.com)`).

----

### Jednorázový příspěvek ###

Pokud chcete jen co nejjednodušeji poslat krátký příspěvek, aniž byste se museli zabývat věcmi okolo, je asi nejpohodlnější napsat mi e-mail na adresu `bsamgtan (zav.) protonmail (t.) com` a do něj přímo vložit samotný překlad, samozřejmě s poznámkou, kam patří nebo co nahrazuje.


### Více příspěvků, na způsob wiki ###

Jestli máte chuť přispět víc, než kolik se přehledně vejde do jednoho e-mailu, nebo chcete vlastnoručně vložit text přímo do souborů, potřebujete si pro tento způsob založit účet na GitLabu. Stačí k tomu platná e-mailová adresa, libovolné uživatelské jméno a fiktivní lidské jméno. Na stránce [Sign in](https://gitlab.com/users/sign_in) vyberte záložku „Register“ a vyplňte údaje.

Následně:

1. Pošlete mi e-mail na adresu `bsamgtan (zav.) protonmail (t.) com`, krátce se představte, ať vím, že nejste robot. Uveďte svoje uživatelské jméno na GitLabu. K uživatelskému účtu vám přidělím práva zápisu do celého projektu afaq-cs.
2. V projektu si otevřete libovolný soubor, například [Sekce A – Co je to anarchismus?](trans/markdown/01_secA.md). Pro úpravu stiskněte tlačítko „Edit“, které se nachází vpravo v hlavičce nad textem. Zobrazí se vám barevně zvýrazněný zdrojový text ve formátu Markdown.
3. Aby se vám text na konci řádku zalamoval, stiskněte v hlavičce tlačítko „Soft wrap“.
4. Přímo v textovém poli můžete překládat.
5. Až s překládáním skončíte, najdete úplně dole na stránce (pod textem) zelené tlačítko „Commit Changes“, kterým změny uložíte. Do políčka „Commit message“ předtím ve zkratce napište, v čem vaše úprava spočívá.
6. Hotovo! Opakujte od bodu 2.


### Ovládáte git přímo? ###

Zaregistrujte se na GitLabu. Vytvořte si vlastní *fork* projektu, v něm překládejte. Pak mi pošlete *merge request*.

Anebo:

1. Pošlete mi e-mail na adresu `bsamgtan (zav.) protonmail (t.) com`, krátce se představte. Uveďte svoje uživatelské jméno na GitLabu. Zmiňte, že si poradíte s gitem.
2. Přidám vás jako člena projektu, takže budete mít rovnou právo zápisu. Posílejte *commity*.

