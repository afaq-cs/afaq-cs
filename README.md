## Anarchismus: Často kladené dotazy ##

Český překlad dokumentu [An Anarchist FAQ](http://anarchism.pageabode.com/afaq/index.html), verze 15.0

### Struktura překládaného textu: ###

- [Úvod](trans/markdown/00_intro.md) *(jen nadpisy)*
- Hlavní část:
   - [Sekce A – Co je to anarchismus?](trans/markdown/01_secA.md) *(rozpracováno: asi 5 z 51, 10 %)*
   - [Sekce B – Proč anarchisté odmítají současný systém?](trans/markdown/02_secB.md) *(jen úvod)*
   - [Sekce C – Jaké jsou mýty kapitalistické ekonomiky?](trans/markdown/03_secC.md) *(jen nadpisy)*
   - [Sekce D – Jak etatismus a kapitalismus ovlivňují společnost?](trans/markdown/04_secD.md) *(jen nadpisy)*
   - [Sekce E – Co je podle anarchistů příčinou ekologických problémů?](trans/markdown/05_secE.md) *(jen nadpisy)*
   - [Sekce F – Je „anarcho“-kapitalismus také druh anarchismu?](trans/markdown/06_secF.md) *(jen nadpisy)*
   - [Sekce G – Je individualistický anarchismus kapitalistický?](trans/markdown/07_secG.md) *(jen nadpisy)*
   - [Sekce H – Proč anarchisté odmítají státní socialismus?](trans/markdown/08_secH.md) *(jen nadpisy)*
   - [Sekce I – Jak by anarchistická společnost vypadala?](trans/markdown/09_secI.md) *(jen nadpisy)*
   - [Sekce J – Co anarchisté dělají?](trans/markdown/10_secJ.md) *(jen nadpisy)*
- Přílohy:
   - [Příloha (A) – Anarchismus a „anarcho“-kapitalismus](trans/markdown/11_appA.md) *(jen nadpisy)*
   - [Příloha (B) – Symboly anarchie](trans/markdown/12_appB.md) *(jen nadpisy)*
   - [Příloha (C) – Anarchismus a marxismus](trans/markdown/13_appC.md) *(jen nadpisy)*
   - [Příloha (D) – Ruská revoluce](trans/markdown/14_appD.md) *(jen nadpisy)*
- [Bibliografie](trans/markdown/15_biblio.md) *(kompletní, ale ze své podstaty převážně anglicky)*

----

### Jak přispět ###

Jakékoliv příspěvky jsou vřele vítány. Pro instrukce viz [návod v souboru CONTRIBUTING.md](CONTRIBUTING.md).

Pro začátek je prioritou přeložit alespoň úvod v každé ze sekcí A až J.


### Licence ###

Na celý tento projekt včetně jakýchkoliv příspěvků se uplatňuje licence [GNU GPL verze 3](LICENSE). Za zdrojový kód se považují soubory ve formátu Markdown (.md) obsahující český překlad.

