# Sekce A – Co je to anarchismus? #

## A.0 Úvod ##

Moderní civilizace čelí třem potenciálně katastrofickým krizím: (1) společenskému zhroucení, zkrácenému termínu pro rostoucí chudobu, bezdomovectví, zločinnost, násilí, odcizení, zneužívání alkoholu a drog, sociální izolaci, politickou apatii, odlidštění, rozpad komunitních struktur zajišťujících vzájemnou pomoc atd.; (2) zničení křehkého ekosystému planety, na němž závisejí veškeré formy života; a (3) rozmnožování zbraní hromadného ničení, zejména jaderných zbraní.

Ortodoxní postoj, jenž zahrnuje názory „expertů“ establishmentu, hlavních médií a politiků obvykle pohlíží na tyto krize jako oddělitelné, každá má své vlastní příčiny, a tedy je možné vyřešit jednu po druhé, v izolaci od ostatních dvou. Je však zřejmé, že tento „ortodoxní“ přístup nefunguje, jelikož se zmíněné problémy zhoršují. Nepřejdeme-li brzy k nějakému lepšímu řešení, očividně míříme ke katastrofě, buď pocházející z ničivé války, ekologické Apokalypsy, nebo pádu do městského divošství – anebo ze všech najednou.

Anarchismus nabízí jednotný a ucelený způsob, jak těmto krizím porozumět, vystopováním jejich společného původu. Tímto zdrojem je princip **hierarchické autority**, na němž spočívají hlavní instituce všech „civilizovaných“ společností, ať už kapitalistických nebo „komunistických“. Anarchistická analýza tedy vychází z faktu, že všechny naše hlavní instituce existují ve formě hierarchií, tzn. organizací, které soustřeďují moc na vrcholku pyramidové struktury, jako například korporace, vládní byrokracie, armády, politické strany, náboženské organizace, univerzity atd. Dále pak rozebírá, jak autoritářské vztahy těmto hierarchiím vlastní negativně ovlivňují jedince, společnost i kulturu. V první části tohoto FAQ (**sekce A až E**) předložíme podrobnější anarchistickou analýzu hierarchické autority a jejích negativních dopadů.

Byla by však chyba se domnívat, že je anarchismus pouze kritika moderní civilizace, výhradně „negativní“ nebo „destruktivní“. Je totiž mnohem víc než to. Jednak je například návrhem svobodné společnosti. To, co můžeme nazvat „anarchistická otázka“, vyjádřila Emma Goldman následovně: *„Problém, jenž před námi dnes stojí … je, jak být sám sebou, a přesto v jednotě s druhými, jak hluboce soucítit se všemi lidskými bytostmi, a zároveň si uchovat své vlastní charakteristické kvality.“* \[**Red Emma Speaks**, s. 158–159\] Jinými slovy, jak můžeme vytvořit společnost, kde se realizuje potenciál každého jednotlivce, ale ne na úkor druhých? Se záměrem tohoto dosáhnout si anarchisté představují společnost, v níž nebudou lidské záležitosti řízeny **„odshora dolů“** skrze hierarchické struktury centralizované moci, nýbrž budou, jak píše Benjamin Tucker, *„spravovány jedinci či dobrovolnými sdruženími.“* \[**Anarchist Reader**, s. 149\] Ačkoliv pozitivní anarchistické návrhy takovéhoto organizování společnosti „zdola nahoru“ budou popisovat teprve pozdější sekce tohoto FAQ (**sekce I a J**), různé prvky konstruktivního jádra anarchismu lze nalézt i v dřívějších sekcích. Pozitivní aspekt anarchismu můžete spatřit i v anarchistické kritice natolik chybných řešení společenské otázky, jako je marxismus a pravicový „libertarianismus“ (v pořadí **sekce F a H**).

Jak to elegantně  vyjádřil Clifford Harper, *„jako všechny velké ideje je i anarchismus v podstatě dost jednoduchý – v lidech se ukazuje to nejlepší, když žijí svobodní od autority a rozhodují si o věcech sami mezi sebou, místo aby si nechali rozkazovat.“* \[**Anarchy: A Graphic Guide**, s. vii\] Anarchisté si v důsledku své touhy maximalizovat individuální, a tím pádem i společenskou svobodu přejí odstranit všechny instituce, které lidi utlačují:

> „Všem anarchistům je společná touha osvobodit společnost od všech politických i sociálních donucovacích institucí, jež stojí v cestě rozvoji svobodného lidstva.“ \[Rudolf Rocker, **Anarcho-Syndicalism**, s. 9\]

Jak uvidíme, všechny takové instituce jsou hierarchie a jejich represivní povaha pramení právě z hierarchické formy.

Anarchismus je socio-ekonomická a politická teorie, nikoliv však ideologie. Tento rozdíl je **velice** důležitý. Teorie znamená, že máte nové myšlenky; ideologie znamená, že myšlenky mají vás. Anarchismus je souborem idejí, které ale jsou pružné, neustále ve stavu vývoje a proměny a ve světle nových dat otevřené úpravám. Jak se mění a vyvíjí společnost, mění se i anarchismus. Oproti tomu ideologie představuje množinu „fixních“ idejí, v něž lidé dogmaticky věří a obvykle přitom skutečnost ignorují nebo ji „pozměňují“, aby jim do ideologie zapadala, protože ta je (z definice) pravdivá. Ze všech takovýchto „fixních“ myšlenek vychází tyranie a rozpory, což vede ke snahám vměstnat každého do stejného Prokrustova lože. Toto bude platit bez ohledu na konkrétní ideologii – leninismus, objektivismus, „libertarianismus“ nebo cokoliv jiného – všechny přinesou stejný účinek: zničení skutečných individuálů ve jménu doktríny, a to takové, která obvykle slouží zájmům nějaké vládnoucí elity. Aneb jak to vyjádřil Michael Bakunin:

> „Lidské dějiny až dosud představují pouze neutuchající krvavé obětovávání milionů ubohých lidí k poctě nějaké nelítostné abstrakce – Boha, země, státní moci, národní hrdosti, historických práv, soudních práv, politické svobody, obecného blaha.“ \[**Bůh a Stát**, s. ?\] <!-- dohledej v českém vydání -->

Dogmata jsou ve své rigiditě statická a smrti podobná, často důsledkem nějakého mrtvého „proroka“, ať už náboženského či sekulárního, jehož následovníci z jeho myšlenek zbudují modlu neměnnou jako kámen. Anarchisté chtějí, aby živí pohřbili mrtvé, ať se můžou živí zabývat vlastními životy. Živí by měli vládnout mrtvým, ne naopak. V tom, jak nabízejí knihu pravidel a „odpovědí“, které z nás snímají břímě samostatného myšlení, jsou ideologie protivníkem kritického uvažování, a tím pádem i svobody.

Při vytváření tohoto FAQ o anarchismu není naším záměrem dát vám „správné“ odpovědi nebo nový zákoník. Krátce vysvětlíme, čím byl anarchismus v minulosti, ale více se zaměříme na jeho moderní podoby a proč jsme nyní **my** anarchisty. Tento FAQ se ve vás snaží podněcovat myšlení a analýzu. Pokud hledáte novou ideologii, pak se omlouváme, ale anarchismus pro vás není.

Ačkoliv se anarchisté snaží být realističtí a praktičtí, nejsme „rozumní“ lidé. Takoví totiž nekriticky přijímají, co jim jako pravdivé sdělí „experti“ a „autority“, a proto navždy zůstanou otroci! Jak napsal Bakunin, anarchisté ví, že:

> „člověk je silný, jen když stojí na své vlastní pravdě, když mluví a jedná ze svých nejhlubších přesvědčení. Pak bez ohledu na situaci, v níž se nachází, vždy ví, co musí říct a udělat. Třeba padne, ale nemůže si na sebe nebo své cíle přivodit hanbu.“ \[cituje Albert Meltzer v **I couldn't Paint Golden Angels**, s. 2\]

Bakunin popisuje moc nezávislého myšlení, což je moc svobody. Vybízíme vás, ať nejste „rozumní“, nepřijímáte, co vám řeknou jiní, ale ať myslíte a jednáte sami za sebe!

A ještě jedna věc: toto samozřejmě **není** poslední slovo, které má o anarchismu padnout. Spousta anarchistů nebude s mnohým zde psaným souhlasit, což se ovšem, když lidé uvažují samostatně, dá čekat. Přejeme si pouze vyznačit **hlavní** ideje anarchismu a podat svou analýzu určitých témat na základě toho, jak chápeme a aplikujeme tyto myšlenky my. Jsme si však jistí, že s klíčovými myšlenkami, které představujeme, budou všichni anarchisté souhlasit, i kdyby měli sem tam výhrady vůči tomu, jak je aplikujeme.


## A.1 Co je to anarchismus? ##

<!-- následující text pochází z wiki provozované dříve ČS anarchistickou federací, archivované v květnu 2011; stránky jsou od policejního zásahu nedostupné; překlad vycházel ze starší verze AFAQ, proto je nutné větu po větě zrevidovat; původní adresa: http://wiki.csaf.cz/afaq:a -->

Anarchismus je politická teorie, jejímž cílem je vytvořit anarchii, „nepřítomnost pána, vládce“ \[P.-J. Proudhon, **What is Property**, s. 264\]. Jinak řečeno, anarchismus je politická teorie, která míří k vytvoření společnosti, v níž spolu jedinci svobodně spolupracují jako sobě rovní. Jako takový tedy anarchismus odporuje všem formám hierarchické vlády – ať už státní či kapitalistické – pro její škodlivost jedinci a individualitě jakož i pro její zbytečnost.

Slovy anarchistky L. Susan Brown:

> „Přestože se běžně vnímá anarchismus jako násilnické protistátní hnutí, jde ve skutečnosti o mnohem propracovanější a rozmanitější tradici než o prostý odpor k vládní moci. Anarchisté nesdílí názor, že jsou moc a nadvláda pro fungování společnosti nutné, a místo toho obhajují kooperativní, proti-hierarchické formy společenské, politické a ekonomické organizace.“ \[**The Politics of Individualism**, s. 106\]

„Anarchismus“ a „anarchie“ každopádně zůstávají nejmylněji vykládané pojmy v politické teorii. Většinou se používají ve smyslu „chaos“ nebo „neuspořádanost“, což naznačuje, že anarchisté touží nastolit společenský chaos a vrátit se k „zákonům džungle“.

V historii není tento proces nesprávného výkladu pojmů ojedinělý. Například v zemích, které považovaly vládu jednoho člověka (monarchii) za nutnost, se slova jako „republika“ nebo „demokracie“ používala přesně ve stejném smyslu jako „anarchie“, k označení nepořádku a zmatku. Ti, kteří mají hmotný zájem na zachování *statutu quo*, budou samozřejmě chtít tvrdit, že odpor k současnému systému nemůže v praxi fungovat a že nová podoba společnosti povede jedině k chaosu. Aneb, jak to vyjádřil Errico Malatesta:

> „jelikož se mělo za to, že je státní správa nezbytná a bez ní by nastal jen zmatek a nepořádek, vyplynulo přirozeně a logicky, že anarchie, která znamená nepřítomnost státní správy, by měla znít jako nepřítomnost řádu.“ \[**Anarchy**, s. 16\]

Anarchisté chtějí tuto „běžnou“ představu „anarchie“ změnit, aby lidé pochopili, že jsou státní správy a další hierarchické vztahy škodlivé **i** zbytečné:

> „Ovlivňujte názory, přesvědčujte veřejnost, že státní správa je nejen zbytečná, ale i krajně škodlivá. A potom slovo anarchie, právě proto, že znamená nepřítomnost státní správy, nabude pro každého význam: přirozený pořádek, jednota lidských potřeb a zájmů všech, úplná svoboda uvnitř úplné solidarity.“ \[**tamtéž**, s. 16\]

Tyto *Často kladené dotazy* spadají do procesu změny běžných představ o anarchismu a významu anarchie. Ale to není vše. Právě tak, jako musíme zápasit s nedorozuměními pocházejícími z „běžné“ představy „anarchie“, musíme se také vypořádat s překrucováním, jemuž jsou anarchismus a anarchisté v průběhu let vystaveni ze strany našich politických a společenských protivníků. Protože, jak to vyjádřil Bartolomeo Vanzetti, anarchisté jsou *„nejradikálnější z radikálů, černé kočky, postrach mnohých, mnoha fanatiků, vykořisťovatelů, šarlatánů, podvodníků a utlačovatelů. Tím pádem jsme také nejvíce očerňovaní, nepochopení, všemi stíhaní a naše slova nejvíce komolená.“* \[Nicola Sacco a Bartolomeo Vanzetti, **The Letters of Sacco and Vanzetti**, s. 274\]

Vanzetti věděl, o čem mluví. On a jeho přítel Nicola Sacco byli americkým státem falešně obviněni za zločin, který nespáchali, a v roce 1927 skončili popraveni na elektrickém křesle fakticky za to, že byli anarchisté z cizí země. Takže tyto *Často kladené dotazy* budou muset strávit nějaký čas napravováním pomluv a zkreslení, kterým anarchisté čelí od kapitalistických médií, politiků, ideologů a šéfů (nemluvě o zkresleních od našich někdejších kolegů radikálů jako jsou liberálové a marxisté). Až skončíme, snad pochopíte, proč mocní vynakládají na útoky proti anarchismu tolik času – je to totiž jediná myšlenka, jež může účinně zajistit svobodu pro všechny a skoncovat se systémy založenými na hrstce lidí ovládající mnohé.


### A.1.1 Co znamená slovo „anarchie“? ###

Slovo „anarchie“ pochází z řečtiny, předpona **an** (nebo také **a**) znamená zápor, „nedostatek čeho“, „nepřítomnost čeho“, „chybění čeho“, k tomu slovo **archos** znamená „vládce“, „řídící“, „velitel“, „vedoucí“, „autorita“. Nebo, jak to vyjádřil Petr Kropotkin, anarchie pochází z řeckých slov znamenajících „protikladný k autoritě.“ \[**Anarchism**, s. 284\].

Ač je řeckým slovům **_anarchos_** a **_anarchia_** často přikládán význam *„nemít žádnou vládu (státní správu)“*, *„být bez vlády“*, původní význam anarchismu, jak jsme si ukázali, není prostě *„žádná vláda“*. **_„An-archie“_** znamená *„bez vládce“* nebo obecněji *„bez autority“*, a právě v tomto významu toto slovo anarchisté odjakživa používají. Například v Kropotkinově díle najdeme obhajobu myšlenky, že anarchismus *„neútočí pouze na kapitál, ale i na hlavní zdroje moci kapitalismu: na zákon, autoritu a stát.“* \[**tamtéž**, s. 150\]. Pro anarchisty znamená anarchie *„ne nutně absenci řádu, jak se obvykle předpokládá, ale absenci panování“*. \[Benjamin Tucker, **Instead of a Book**, s. 13\] Znamenitě shrnuje David Weick:

> „Anarchismus se dá chápat jako obecná sociální a politickou myšlenka, která vyjadřuje negaci veškeré síly, svrchovanosti, dominance a hierarchického rozdělení a rovněž vůli k jejich ukončení. Tudíž je anarchismus více než protistátnost… \[i přesto že\] vláda (stát)… je náležitě hlavním předmětem anarchistické kritiky.“ \[**Reinventing Anarchy**, s. 139\]

Z těchto důvodů není anarchismus ani tolik protivládní a protistátní, jako je primárně hnutím proti **hierarchii**. Proč? Protože hierarchie je organizační struktura, která autoritu ztělesňuje. A jelikož je stát „nejvyšší“ forma hierarchie, jsou anarchisté podle definice protistátní; na dostatečnou definici anarchismu to však **nestačí**. Znamená to, že se opravdoví anarchisté staví proti všem formám hierarchické organizace, ne jen proti státu. Slovy Briana Morrise:

> „Pojem anarchie pochází z řečtiny a v podstatě znamená „žádný vládce“. Anarchisté jsou lidé, kteří odmítají všechny formy vlády nebo nátlakové autority, všechny formy hierarchie a ovládání. Tudíž odporují tomu, co mexický anarchista Flores Magón nazval „chmurnou trojicí“: státu, kapitalismu a církvi. Anarchisté se tedy staví proti kapitalismu i proti státu, stejně jako proti všem formám náboženské autority. Ale anarchisté chtějí také různými prostředky nastolit či vybudovat anarchii, tedy decentralizovanou společnost bez nátlakových institucí, společnost organizovanou prostřednictvím federace dobrovolných sdružení.“ \[„*Anthropology and Anarchism*“, s. 35–41, **Anarchy: A Journal of Desire Armed**, č. 45, s. 38\]

Odkaz na „hierarchii“ v tomto kontextu vychází teprve z nedávného vývoje – „klasičtí“ anarchisté jako Proudhon, Bakunin nebo Kropotkin toto slovo sice používali, ale jen zřídka (obvykle dávali přednost výrazu „autorita“, jenž sloužil jako zkratka pro „autoritářský“). Nicméně z jejich děl je jasné, že zastávali filozofii proti hierarchii, proti každé nerovnosti moci a privilegiím jednotlivců. O tomto Bakunin mluvil, když napadal *„oficiální“* autoritu, ale hájil *„přirozený vliv“*, a také, když řekl:

> „Chcete zajistit, aby nemohl nikdo utlačovat svého druha? Pak udělejte vše pro to, aby nikomu nepatřila moc.“ \[**The Political Philosophy of Bakunin**, s. 271\]

Jak poznamenává Jeff Draughn, „přestože širší koncept odporu proti hierarchii byl latentní součástí ‚revolučního projektu‘ už od začátku, teprve v poslední době se mu začalo dostávat konkrétního pečlivého zkoumání. Každopádně to můžeme snadno spatřit v řeckém původu slova ‚anarchie‘.“ \[**Between Anarchism and Libertarianism: Defining a New Movement**\]

Zdůrazňujeme, že se pro anarchisty toto odmítání hierarchie neomezuje pouze na stát nebo vládu. Zahrnuje všechny autoritářské ekonomické a společenské vztahy, rovněž politické, zejména ty spojené s kapitalistickým vlastnictvím a námezdní prací. Toho si můžeme povšimnout v Proudhonově argumentaci, že *„**Kapitál**… je v politické oblasti analogický k vládě… Ekonomická myšlenka kapitalismu, politika vlády či jiné autority a teologická idea církve jsou tři totožné myšlenky, různými způsoby provázané. Útočit na jednu z nich znamená útočit na všechny… Co činí kapitál pracovní síle a stát svobodě, to dělá i církev duchu. Tato trojice absolutismu je stejně zhoubná v praxi, jako je zhoubná její filozofie. Nejúčinnější způsob, jak utlačovat lid, je současně zotročit jeho tělo, vůli i rozum.“* \[cituje Max Nettlau, **A Short History of Anarchism**, s. 43–44\] Podobným způsobem Emma Goldman odmítá kapitalismus, protože v něm *„člověk musí prodat svou pracovní sílu“*, a tudíž *„jsou jeho \[nebo její\] sklony a úsudek podřízeny vůli pána.“* \[**Red Emma Speaks**, s. 50\] O čtyřicet let dříve ve stejném smyslu prohlásil Bakunin, že ve stávajícím systému *„zaměstnanec na daný čas prodává svou osobu a svou svobodu“* kapitalistovi výměnou za mzdu. \[**tamtéž**, s. 187\]

„Anarchie“ tedy znamená více než prostě „bezvládí“, znamená opozici ke všem formám autoritářské organizace a hierarchie. Kropotkinovými slovy: *„původ anarchistického pojetí společnosti… \[spočívá v\] kritice… hierarchických organizací a autoritářských koncepcí společnosti; a… v analýze tendencí pozorovatelných v pokrokových hnutích lidstva.“* \[**tamtéž**, s. 158\]. Dle Malatesty se anarchismus *„zrodil v morální vzpouře proti sociální nespravedlnosti“* a *„konkrétní příčiny společenských neduhů“* se dají nalézt v *„kapitalistickém vlastnictví a státě.“* Když utlačovaný *„usiloval svrhnout stát i vlastnictví – tehdy se zrodil anarchismus.“* \[**Errico Malatesta: His Life and Ideas**, s. 19\]

Snaha tvrdit, že je anarchie čistě protistátní, je dezinterpretace pojmu i způsobu, kterým jej používá anarchistické hnutí. Jak zdůvodňuje Brian Morris, *„když prozkoumáte spisy klasických anarchistů… a rovněž charakter anarchistického hnutí… je zcela zřejmé, že se anarchismus nikdy ve své vizi takto úzce nevymezoval \[jen proti státu\]. Vždy zpochybňoval všechny formy autority a vykořisťování a kritizoval do stejné míry kapitalismus a náboženství, jako se stavěl proti státu.“* \[**tamtéž**, s. 40\]

A pro jistotu ještě připomeneme, že anarchie neznamená chaos a anarchisté neusilují o vytváření chaosu či nepořádku. Místo toho si přejeme vybudovat společnost založenou na osobní svobodě a dobrovolné spolupráci. Jinými slovy, řád zdola směrem vzhůru, nikoliv nepořádek vnucený shora autoritami. Taková společnost by byla skutečnou anarchií, společností bez vládců.

Ačkoliv probíráme, jak by anarchistická společnost mohla vypadat, až v [sekci I](09_secI.md), její klíčový aspekt vystihuje Noam Chomsky, když říká, že v opravdu svobodné společnosti *„každou interakci mezi lidskými bytostmi, která je více než osobní – tedy má některou institucionální formu – v komunitě, na pracovišti, v rodině, v širší společnosti nebo kdekoliv jinde, by tuto interakci měli mít ve svých rukou přímo její účastníci. To znamená rady pracujících v průmyslu, lidovou demokracii v komunitách, interakci mezi nimi, svobodné asociace ve větších skupinách a tak dále, až k organizování mezinárodní společnosti.“* \[**Anarchism Interview**\] Společnost by se již nedělila na hierarchii šéfů a pracujících, vládců a ovládaných. Místo toho by anarchistická společnost byla založena na svobodném sdružení v participačních organizacích a řízena zdola směrem vzhůru. Nutno zmínit, že anarchisté se snaží ve svých organizacích, zápasech a činnostech už dnes vytvářet z této společnosti vytvářet nejvíce, kolik mohou.


### A.1.2 Co znamená slovo „anarchismus“? ###

Anarchismus je, slovy Petra Kropotkina, *„nevládní systém socialismu.“* \[**Anarchism**, s. 46\] Jinak řečeno, *„odstranění vykořisťování a utlačování člověka člověkem, to jest zrušení soukromého vlastnictví \[tzn. kapitalismu\] a státní správy.“* \[Errico Malatesta, **Towards Anarchism**, s. 75\]

Anarchismus je tedy politická teorie, která usiluje o vytvoření společnosti bez politických, ekonomických a společenských hierarchií. Anarchisté zastávají názor, že anarchie, absence vládců, je životaschopná forma společenského systému, a tak pracují na maximalizaci individuální svobody a společenské rovnosti. Cíle svobody a rovnosti se podle nich navzájem podporují. Aneb, jak zní Bakuninovo slavné prohlášení:

> „Jsme přesvědčeni, že svoboda bez socialismu je výsadou a nespravedlností, a že socialismus bez svobody je otroctvím a surovostí.“ \[**The Political Philosophy of Bakunin**, s. 269\]

Dějiny lidské společnosti k tomuto argumentu poskytují důkaz. Svoboda bez rovnosti je jen svobodou pro mocné, zatímco rovnost bez svobody je neproveditelná a ospravedlňuje otroctví.

Ačkoliv existuje mnoho odlišných druhů anarchismu (od individualistického anarchismu až po anarchismus komunistický – viz sekci A.3), všechny vždy v jádru sdílely dvě společná stanoviska – opozici k vládě a opozici ke kapitalismu. Slovy individualistického anarchisty Benjamina Tuckera anarchismus trvá *„na zrušení státu a zrušení lichvy; na ustání vlády člověka nad člověkem a vykořisťování člověka člověkem.“* \[cituje Eunice Schuster, **Native American Anarchism**, s. 140\] Všichni anarchisté vnímají zisk, úrok a nájem jako **lichvu** (t.j. vykořisťování), a proto je i podmínky, které jim dávají vzniknout, odmítají do stejné míry, jako odmítají vládu a stát.

Obecněji vzato slovy L. Susan Brown: *„sjednocující článek“* uvnitř anarchismu *„je všeobecné odsouzení hierarchie a nadvlády a ochota bojovat za svobodu lidského jedince.“* \[**The Politics of Individualism**, s. 108\] Podle anarchistů člověk nemůže být svobodný, jestliže podléhá státní nebo kapitalistické autoritě. Jak shrnula Voltairine de Cleyre:

> „Anarchismus učí o možné společnosti, v níž mohou být všem plně poskytnuty životní potřeby a v níž každý dědictvím získává příležitost na úplné rozvinutí mysli i těla… Učí, že současná nespravedlivá organizace výroby a přerozdělování bohatství musí být konečně zcela zničena a nahrazena systémem, který zajistí každému svobodu pracovat, bez nutnosti si nejprve hledat pána, jemuž je třeba odevzdávat desátek ze svého úsilí; který zaručí každému svobodný přístup ke zdrojům a výrobním prostředkům… Ze slepě podřízených činí nespokojené; z nevědomě nespokojených vědomě nespokojené… Anarchismus usiluje vzbudit vědomí o útlaku, touhu po lepší společnosti a tušení o nutnosti neustávajícího válčení proti kapitalismu i státu.“ \[**Anarchy! An Anthology of Emma Goldman's Mother Earth**, s. 23–24\]

Takže anarchismus je politická teorie, která obhajuje vytvoření anarchie, společnosti založené na etické zásadě *„žádní vládci“*. Za tímto účelem *„společně se socialisty anarchisté zastávají názor, že soukromé vlastnictví půdy, kapitálu a strojů už zůstává přes čas; že je odsouzeno vymizet: a že se všechny předpoklady výroby musejí stát, a také stanou, společným majetkem společnosti a budou kolektivně spravovány výrobci bohatství. A… trvají na tom, že ideál politické organizace společnosti je stav věcí, kde zůstávají funkce vlády omezeny na minimum… \[a\] že konečným záměrem společnosti je zmenšení těchto funkcí až do vymizení – tedy až ke společnosti bez státní správy, k an-archii.“* \[Petr Kropotkin, **tamtéž**, s. 46\]

Tím pádem je anarchismus pozitivní i negativní. Rozebírá a kritizuje současnou společnost a zároveň přitom nabízí představu potenciální nové společnost – takové, která naplňuje jisté lidské potřeby, jež ta nynější odpírá. Tyto potřeby jsou ve svém základu svoboda, rovnost a solidarita, o čemž pojednáváme v sekci A.2 \[8\].

Anarchismus spojuje kritickou analýzu s nadějí, neboť, jak poukázal Bakunin (ještě ve své před-anarchistické době), *„nutkání ničit je tvůrčí nutkání.“* Není možné vybudovat lepší společnost, aniž bychom porozuměli, proč je špatná ta stávající.

Je však nutné zdůraznit, že anarchismus je více než pouhý prostředek analýzy nebo vize lepší společnost. Zakládá se také v úsilí, úsilí utlačovaných o svou svobodu. Jinými slovy, poskytuje prostředek k dosažení nového systému založeného na potřebách lidu, ne moci, a upřednostňujícího planetu před ziskem. Citujme skotského anarchistu Stuarta Christie:

> „Anarchismus je hnutí za lidskou svobodu. Je konkrétní, demokratický a rovnostářský… Anarchismus vznikl jako – a zůstává – přímá výzva znevýhodněných proti jejich utlačování a vykořisťování. Odporuje jak zákeřnému růstu státní moci, tak i zhoubnému charakteru majetnického individualismu, které společně nebo i odděleně vposledku slouží pouze zájmům několika na úkor ostatních.

> Anarchismus je teorie i životní praxe. Filozoficky vzato usiluje o nejvyšší soulad mezi jedincem, společností a přírodou. Prakticky pak má za cíl, abychom se organizovali a žili život tak, že se tím stanou politici, vlády, státy a jejich představitelé nadbyteční. V anarchistické společnosti by se vzájemně respektující svrchovaní jedinci organizovali do nenátlakových vztahů uvnitř přirozeně ohraničených komunit, v nichž jsou výrobní a rozdělovací prostředky vlastněny společně.

> Anarchisté nejsou snílci posedlí abstraktními principy a teoretickými konstrukty… Anarchisté si dobře uvědomují, že dokonalou společnost nemůžeme získat hned zítra. Zápas samozřejmě trvá navěky! Ona vize ovšem poskytuje pobídku bojovat proti věcem, jak jsou, a za věci, které by mohly být…

> Konečně pouze zápas určuje výsledek, a pokrok směrem ke smysluplnější komunitě musí začít s vůlí odolat každé formě nespravedlnosti. V obecných pojmech to znamená napadat veškeré vykořisťování a vzdorovat legitimitě veškeré nátlakové autority. Mají-li anarchisté jediný článek neotřesitelné víry, pak je to ten, že jakmile se jednou ztratí zvyk podřizovat se politikům a ideologům a osvojí se zvyk odporu vůči nadvládě a vykořisťování, mají obyčejní lidé schopnost organizovat si každý aspekt svých životů ve vlastním zájmu, kdekoliv a kdykoliv, svobodně i čestně.

> Anarchisté nestojí stranou lidového boje, ani se jej nepokoušejí ovládnout. Snaží se prakticky přispět, čímkoliv mohou, a také v rámci něj vypomáhat co nejvyšší míře individuálního seberozvoje i skupinové solidarity. Anarchistické myšlenky týkající se dobrovolných vztahů, rovnostářské participace na rozhodovacím procesu a vzájemné pomoci a související kritiku všech forem nadvlády je možné rozpoznat ve filozofických, sociálních a revolučních hnutích všech časů a míst.“ \[**My Granny made me an Anarchist**, s. 162–163\]

Anarchismus, jak tvrdí anarchisté, je jednoduše teoretickým vyjádřením naší schopnosti sami se organizovat a provozovat společnost bez šéfů a politiků. Umožňuje pracujícím a dalším utlačovaným lidem si uvědomit naši společnou moc jakožto třídy, hájit naše bezprostřední zájmy a bojovat za revolucionizaci celé společnosti. Pouze tímto můžeme vytvořit společnost vhodnou k tomu, aby v ní žili lidé.

Není to žádná abstraktní filozofie. Anarchistické myšlenky se každý den uvádějí do praxe. Kdekoli utlačovaní povstanou za svá práva, chopí se akce, aby bránili svou svobodu, praktikují solidaritu a spolupráci, bojují proti utlačování, organizují se bez vůdců a šéfů, tam všude žije duch anarchismu. Anarchisté jednoduše usilují o posílení těchto libertariánských tendencí a o jejich přivedení k plnému uskutečnění. Jak probíráme v [sekci J](10_secJ.md) \[53\], anarchisté své myšlenky v kapitalismu uplatňují mnoha způsoby, aby jej změnili k lepšímu, dokud se jej nezbavíme úplně. [Sekce I](09_secI.md) \[52\] pojednává, čím jej máme v úmyslu nahradit, tj. o co anarchismus usiluje. 


### A.1.3 Proč se anarchismu také říká libertariánský socialismus? ###

Vzhledem k tomu, jak negativní povahu má definice *„anarchismu“*, používá mnoho anarchistů jiné termíny, aby tím zdůraznili nedílnou pozitivní a konstruktivní stránku svých myšlenek. Mezi nejčastější užívané pojmy patří *„svobodný socialismus“*, *„svobodný komunismus“*, „libertariánský socialismus“* a *„libertariánský komunismus“*. Anarchisté vnímají libertariánský socialismus, libertariánský komunismus a anarchismus jako v podstatě zaměnitelné. Jak to vyjádřil Vanzeti:

> „Nakonec jsme přece socialisté, jako sociální demokraté, socialisté, komunisté a I.W.W. jsou všichni Socialisté. Rozdíl – ten zásadní – mezi námi a ostatními spočívá v tom, že jsou autoritářští, zatímco my jsme libertariánští; oni mají důvěru ve svůj vlastní stát nebo vládu; my žádný stát ani vládu neuznáváme.“ \[Nicola Sacco a Bartolomeo Vanzetti, **The Letters of Sacco and Vanzetti**, s. 274\]

Je to ale skutečně tak? Vezměme v úvahu slovník **American Heritage Dictionary**, kde nacházíme definice:

> **LIBERTARIÁN**: ten, kdo věří ve svobodu činu i myšlenky; ten, kdo věří ve svobodnou vůli.

> **SOCIALISMUS**: společenský systém, v němž výrobci vlastní politickou moc i prostředky výroby a rozdělování statků.

Prostým spojením prvních dvou definic tak získáme:

> **LIBERTARIÁNSKÝ SOCIALISMUS**: společenský systém, který uznává svobodu činu i myšlenky a svobodnou vůli a v němž výrobci vlastní politickou moc i prostředky výroby a rozdělování statků.

(Ačkoliv musíme dodat, že stále platí naše obvyklé poznámky ohledně nedostatečné politické sofistikovanosti slovníků. Tyto definice využíváme pouze na důkaz, že „libertariánský“ neznamená nutně „volnotržní“ kapitalismus a „socialismus“ zase státní vlastnictví. Další slovníky budou samozřejmě obsahovat odlišné výklady – obzvlášť socialismu. Ti, kteří mají zájem o slovníkových definicích diskutovat, se tomuto nekončícímu a politicky zbytečnému koníčku věnovat mohou, my však nebudeme.)

Avšak vzhledem k vytvoření americké Libertariánské strany dnes mnoho lidí považuje ideu *„libertariánského socialismu“* za vzájemně se popírající pojmy. Dokonce mnoho „libertariánů“ se domnívá, že se anarchisté prostě pokoušejí „antilibertariánské“ myšlenky „socialismu“ (jak si jej libertariáni představují) spojit s libertariánskou ideologií, aby učinili tyto „socialistické“ myšlenky „přijatelnější“ – jinak řečeno, snaží se „libertariánskou“ nálepku ukrást jejím právoplatným vlastníkům.

Nic nemůže být dále od pravdy. Termínem „libertarián“ anarchisté popisují sebe a své myšlenky již od 50. let 19. století. Podle anarchistického historika Maxe Nettlau publikoval mezi lety 1858 a 1861 v New Yorku revoluční anarchista Joseph Dejacque noviny **Le Libertaire, Journal du Mouvement Social**, zatímco použití termínu „libertariánský komunismus“ sahá do listopadu 1880, kdy jej francouzský anarchistický kongres přijal za vlastní. \[Max Nettlau, **A Short History of Anarchism**, s. 75 a s. 145\] Používání pojmu „libertarián“ se mezi anarchisty začalo těšit větší oblibě od 90. let 19. století po tom, co byl užíván ve Francii ve snaze obejít protianarchistické zákony a vyhnout se negativním konotacím slova „anarchie“ v lidovém povědomí (například Sebastien Faure a Louise Michel vydávali roku 1895 ve Francii list **Le Libertaire – Libertarián**). Od té doby, zejména mimo Ameriku, již byl vždy spojován s anarchistickými idejemi a hnutími. Na příkladu z nedávnější doby: v červenci 1954 v USA anarchisté zorganizovali **_„The Libertarian League“_** („Libertariánskou ligu“), jež se držela skalních anarchosyndikalistických principů a přetrvala až do roku 1965. Oproti tomu v USA působící „Libertariánská“ strana existuje teprve od počátku 70. let 20. století, dobře přes 100 let ode dne, kdy termín poprvé k označení svých politických myšlenek použili anarchisté (a 90 let od prvního přijetí výrazu „libertariánský komunismus“). Právě tato strana, nikoliv anarchisté, slovo „ukradla“. Později v [Sekci B](02_secB.md) \[54\] budeme probírat, proč je vnitřním protimluvem idea „libertariánského“ kapitalismu (jak si jej přeje Libertariánská strana).

Jak také vysvětlíme v [Sekci I](09_secI.md) \[52\], individuální svobodu může maximalizovat pouze libertariánsko-socialistický systém vlastnictví. Státní vlastnictví – to, čemu se běžně **říká** „socialismus“ – samozřejmě pro anarchisty ani zdaleka socialismem není. Dokonce, což rozvedeme v [Sekci H](09_secH.md) \[55\], je státní „socialismus“ pouze formou kapitalismu bez sebemenšího socialistického obsahu. Rudolf Rocker to vyjádřil, když řekl, že pro anarchisty není socialismus *„prostou otázkou plného břicha, nýbrž otázkou kultury, která by měla získat smysl pro osobnost a svobodnou iniciativu jednotlivce; bez svobody by směřovala jen k bezútěšnému státnímu kapitalismu, jenž obětuje veškeré individuální myšlení a cítění fiktivnímu kolektivnímu zájmu.“* \[cituje Colin Ward, *„Introduction“*, Rudolf Rocker, **The London Years**, s. 1\]

Vzhledem k anarchistickému rodokmenu slova „libertarián“ má málokterý anarchista radost pohledět, jak jej zcizila ideologie sdílející s našimi myšlenkami tak málo. Murray Bookchin poznamenal, že ve Spojených státech *„samotný termín ‚libertarián‘ bezpochyby přináší problém, a to sice zavádějící ztotožnění antiautoritářské ideologie s roztroušeným hnutím za ‚čistý kapitalismus‘ a ‚svobodný obchod‘. Toto hnutí nikdy pojem nevytvořilo: přisvojilo si jej z anarchistického hnutí \[devatenáctého\] století. A nazpět by si jej měli dobýt tito antiautoritáři… kteří se snaží mluvit za ovládané lidi jako celek, ne za soukromé egoisty, kteří svobodu ztotožňují s podnikáním a se ziskem.“* Proto by měli v Americe anarchisté *„v praxi obnovit tradici, kterou zbavila přirozenosti“* volnotržní pravice. \[**The Modern Crisis**\], s. 154–155\] A jelikož toto děláme, budeme nadále své myšlenky nazývat libertariánský socialismus.


### A.1.4 Jsou anarchisté také socialisty? ###

Ano. Všechna odvětví anarchismu se staví proti kapitalismu, protože se zakládá na utlačování a vykořisťování (viz sekce [B](02_secB.md) \[54\] a [C](03_secC.md) \[56\]). Anarchisté odmítají *„názor, že lidé nemohou společně pracovat, jestliže nemají vedoucího pána, který by vybral procentní podíl z jejich výrobků“* a domnívají se, že v anarchistické společnosti *„si opravdoví pracovníci vytvoří své vlastní směrnice, rozhodnou, kdy a kde a jak se věci mají udělat.“* Takto se dělníci sami osvobodí *„ze strašlivého nevolnictví kapitalismu.“* \[Voltairine de Cleyre, *„Anarchism“*, **Exquisite Rebel**, s. 75 a 79\]

(Musíme zde zdůraznit, že anarchisté oponují **všem** ekonomickým formám, které se zakládají na nadvládě a vykořisťování, což zahrnuje feudalismus, „socialismus“ na sovětský způsob – lépe nazývaný státní kapitalismus –, otroctví atd. Soustřeďujeme se na kapitalismus, protože je tím, co v současnosti vládne světu.)

Individualisté jako Benjamin Tucker i sociální anarchisté jako Proudhon a Bakunin se prohlašovali za **_„socialisty“_**. Kropotkin ve své klasické eseji *„Moderní věda a anarchie“* vysvětluje, že tak činili, protože: *„dokud se socialismus chápal ve svém širokém, všeobecném a pravdivém smyslu – tedy jako úsilí __zrušit__ vykořisťování pracujících kapitálem –, pochodovali anarchisté s tehdejšími socialisty ruku v ruce.“* \[**Evolution and Environment**, s. 81\] Nebo Tuckerovými slovy, *„základní tvrzení socialismu \[je\], že pracujícím by mělo být dáno vlastnictví sebe samých,“* postoj na němž se *„obě školy socialistického myšlení… státní socialismus a anarchismus“* shodly. \[**The Anarchist Reader**, s. 144\] Tím pádem slovo *„socialista“* v původní definici zahrnovalo *„všechny, kteří věří v právo jedince vlastnit, co sám nebo sama vyrobil/a.“* \[Lance Klafta, *„Ayn Rand and the Perversion of Libertarianism“* v **Anarchy: A Journal of Desire Armed**, č. 34\] Tuto opozici k vykořisťování (či lichvě) sdílejí všichni praví anarchisté a staví se tím pod socialistickou standartu.

Pro většinu socialistů *„jediná záruka, že nebudete okrádáni o plody své práce, je vlastnit pracovní nástroje.“* \[Petr Kropotkin, **Dobývání chleba**, s. 145 v anglickém vydání\] Z tohoto důvodu například Proudhon podporoval dělnická družstva, kde *„má každý jedinec zapojený do spolku… plný podíl na majetku celé společnosti,“* protože *„účastí na ztrátách a ziscích… kolektivní síla \[tj. nadhodnota\] přestává být zdrojem zisků pro několik málo vedoucích: stává se z ní vlastnictví všech pracujících.“* \[**General Idea of the Revolution**, s. 222 a s. 223\] Takže kromě toho, že touží po konci vykořisťování pracujících ze strany kapitálu, si praví socialisté přejí také společnost, v níž výrobci vlastní a ovládají výrobní prostředky (nutno zdůraznit, že to zahrnuje i pracoviště dodávající služby). O tom, jakými prostředky toho výrobci dosáhnou, se v anarchistických a dalších socialistických kruzích vedou spory, avšak cíl zůstává společný. Anarchisté upřednostňují přímou moc pracujících a buď vlastnictví skrze spolky pracujících, nebo komunu (k různým druhům anarchistů viz sekci A.3 \[29\]).


### A.1.5 Odkud anarchismus pochází? ###


## A.2 Co anarchismus zastává? ##


### A.2.1 Co je jádro anarchismu? ###


### A.2.2 Proč anarchisté zdůrazňují svobodu? ###


### A.2.3 Jsou anarchisté pro organizování? ###


### A.2.4 Jsou anarchisté pro „absolutní“ svobodu? ###


### A.2.5 Proč jsou anarchisté pro rovnost? ###


### A.2.6 Proč je pro anarchisty tolik důležitá solidarita? ###


### A.2.7 Proč obhajují anarchisté sebe-osvobození? ###


### A.2.8 Je možné být anarchista a přitom neodmítat hierarchii? ###


### A.2.9 Jaký druh společnosti anarchisté chtějí? ###


### A.2.10 Co bude odstranění hierarchie znamenat a čeho se tím dosáhne? ###


### A.2.11 Proč je většina anarchistů pro přímou demokracii? ###


### A.2.12 Je alternativou k přímé demokracii konsenzus? ###


### A.2.13 Jsou anarchisté individualisté, nebo kolektivisté? ###


### A.2.14 Proč voluntarismus nestačí? ###


### A.2.15 A co „lidská přirozenost“? ###


### A.2.16 Vyžaduje anarchismus „dokonalé“ lidi, aby fungoval? ###


### A.2.17 Není na to, aby svobodná společnost fungovala, většina lidí příliš hloupá? ###


### A.2.18 Podporují anarchisté terorismus? ###


### A.2.19 Jaké etické postoje anarchisté zastávají? ###


### A.2.20 Proč je většina anarchistů ateistická? ###


## A.3 Jaké jsou druhy anarchismu? ##


### A.3.1 Jaké jsou rozdíly mezi individualistickými a sociálními anarchisty? ###


### A.3.2 Dělí se sociální anarchisté na více druhů? ###


### A.3.3 Jaké jsou druhy zeleného anarchismu? ###


### A.3.4 Je anarchismus pacifistický? ###


### A.3.5 Co je to anarcha-feminismus? ###


### A.3.6 Co je to kulturní anarchismus? ###


### A.3.7 Jsou i věřící anarchisté? ###


### A.3.8 Co je to „anarchismus bez přívlastků“? ###


### A.3.9 Co je to anarcho-primitivismus? ###


## A.4 Kdo patří mezi hlavní anarchistické myslitele? ##


### A.4.1 Jsou někteří myslitelé anarchismu blízko? ###


### A.4.2 Jsou blízko anarchismu někteří liberální myslitelé? ###


### A.4.3 Jsou blízko anarchismu někteří socialističtí myslitelé? ###


### A.4.4 Jsou blízko anarchismu někteří marxističtí myslitelé? ###


## A.5 Jaké jsou příklady „Anarchie v akci“? ##


### A.5.1 Pařížská komuna ###


### A.5.2 Haymarketští mučedníci ###


### A.5.3 Budování syndikalistických odborů ###


### A.5.4 Anarchisté za Ruské revoluce ###


### A.5.5 Anarchisté za obsazování italských továren ###


### A.5.6 Anarchisté za Španělské revoluce ###


### A.5.7 Květnová-červnová revolta ve Francii roku 1968 ###

