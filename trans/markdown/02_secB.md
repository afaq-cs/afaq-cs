# Sekce B – Proč anarchisté odmítají současný systém? #


## B.0 Úvod sekce B ##

Tato sekce Často kladených dotazů představuje analýzu základních sociálních vztahů v moderní společnosti a struktur, které je vytvářejí, zejména těch stránek společnosti, jež chtějí anarchisté změnit.

Anarchismus je ve své podstatě vzpoura proti kapitalismu. Jako politická teorie se zrodil ve stejný čas s kapitalismem a v opozici vůči němu. Jako společenské hnutí nabýval síly a vlivu, zatímco kapitalismus kolonizoval čím dál více částí společnosti. Nevyjadřuje jen prosté odmítání státu, jak tvrdí někteří takzvaní experti, nýbrž vždy odporoval dalším formám autority a útlaku z nich pocházejícímu, zejména kapitalismu a jeho specifické podobě soukromého vlastnictví. Není náhodou, že se Proudhon jako vůbec první prohlásil za anarchistu právě v knize nazvané **Co je vlastnictví?** (a podal odpověď **„Je to krádež!“**). Od Proudhona dále se anarchismus vždy stavěl jak proti státu, tak i proti kapitalismu (dokonce jde o jediné, na čem se shodli tak rozdílní myslitelé jako Benjamin Tucker a Petr Kropotkin). Od Proudhonových časů ovšem anarchismus svou kritiku autority rozšířil za hranice těchto dvou společenských zel. Zavrhnutí se dostalo například sexismu, rasismu a homofobii, jakožto omezením svobody a rovnosti. Tato sekce Často kladených dotazů tedy shrnuje klíčové myšlenky v pozadí anarchistického odmítání současného systému, v němž žijeme.

To samozřejmě neznamená, že před vznikem kapitalismu ve společnosti anarchistické myšlenky neexistovaly. Zdaleka ne. Myslitelé, jejichž úvahy se dají zařadit jako anarchistické, sahají tisíce let do minulosti a vyskytují se v mnoha rozličných kulturách a oblastech. Nemusíme ani přehánět, když řekneme, že se anarchismus zrodil v okamžiku, kdy byl vytvořen stát a soukromé vlastnictví. Jak ale poznamenal Kropotkin, ačkoliv *„za všech časů byli anarchisté a etatisté,“* v naší době *„anarchii přinesl stejný kritický a revoluční protest, z nějž povstal socialismus jako takový.“* Avšak na rozdíl od jiných socialistů se anarchisté nezastavují u *„negace kapitalismu a společnosti založené na podřazení pracovní síly kapitálu“* a dále *„se vyhlašují proti tomu, co utváří pravou sílu kapitalismu: proti státu a jeho zásadním oporám – centralizaci autority, zákonu, vždy vytvořenému menšinou za účelem jejího zisku, a formě spravedlnosti, jejíž hlavní cíl spočívá v ochraňování autority a kapitalismu.“* Tudíž anarchismus nestál *„jen proti kapitalismu samému, ale také proti těmto pilířům kapitalismu: zákonu, autoritě a státu.“* \[**Evolution and Environment**, s. 16 a s. 19\]

Jinými slovy anarchismus, jak dnes existuje, jako sociální hnutí s dlouhou historií zápasu, s politickou teorií a souborem myšlenek, je výsledkem společenské transformace, která provázela vznik moderního (národního) státu a kapitálu, a (což je mnohem důležitější) reakce, odpor a opozice těch, kteří těmto novým sociálním vztahům a institucím podléhají. Tím pádem se analýza a kritika v této sekci Často kladených dotazů předložená zaměří na moderní, kapitalistickou společnost.

Anarchisté si uvědomují, že moc vlád a dalších druhů hierarchie závisí na souhlasu ovládaných. Strach neposkytuje plné vysvětlení, jde mnohem více o to, že *„\[utlačovaní\] zastávají stejné hodnoty jako jejich vládci. Panovníci i opanovaní uznávají princip autority, hierarchie, moci.“* \[Colin Ward, **Anarchy in Action**, s. 15\] S ohledem na to nabízíme v této sekci Často kladených dotazů své argumenty, abychom takový „konsenzus“ zpochybnili, abychom přednesli, proč se máme stát anarchisty, a proč autoritářské sociální vztahy a organizace nejsou v našem zájmu.

Netřeba připomínat, že taková úloha není snadná. Každá vládnoucí třída může přežívat, jedině pokud lidé, kteří podléhají institucím dávajícím jí moc, tyto instituce celkově schvalují. Toho se dosahuje rozličnými prostředky – propagandou, takzvaným vzdělávacím systémem, tradicí, médii, obecnými kulturními předpoklady společnosti. Takovým způsobem se pak dominantními myšlenkami ve společnosti stávají myšlenky dominantní elity. Což znamená, že každé sociální hnutí musí, než se je pokusí ukončit, s těmito myšlenkami bojovat:

> *„Lidé často ani nerozpoznají existenci systémů útlaku a dominance. Aby vůbec začali vnímat represi, musejí nejdříve v systémech, v nichž žijí, zkusit zápasit o svá práva. Vezměte si ženské hnutí. Jedním z prvních kroků v jeho vývoji byly takzvané ‚snahy pozvednout uvědomění‘. Snažit se, aby ženy poznaly, že jejich podřazenost a ovládanost není přirozený stav světa. Má babička se k ženskému hnutí nemohla přidat, jelikož v určitém smyslu necítila žádný útlak. Takový prostě život byl, jako když ráno vychází slunce. Dokud si lidé neuvědomí, že to není jako východ slunce, že se to dá změnit, že nemusejí poslouchat příkazy, že se nemusejí nechat bít, dokud lidé nedovedou vnímat, že je na tom něco špatného, dokud to není přemoženo, není možné pokračovat. A jednou možností, jak toho dosáhnout, je snažit se protlačovat reformy v rámci stávajících systémů represe, a dříve či později zjistíte, že je musíte změnit.“* \[Noam Chomsky, **Anarchism Interview**\]

To znamená, jak zdůraznil Malatesta, že anarchistovou *„první úlohou musí být přesvědčit lid.“* Tedy musíme *„přivádět lidi k uvědomění si neštěstí, jimiž trpí, a svých šancí je zničit… Prochladlým a hladovějícím předvedeme, jak možné a snadné by bylo každému zajistit hmotné potřeby. Utlačovaným a opovrhovaným ukážeme, jak by se dalo žít šťastně ve světě lidí, kteří jsou si rovní a svobodní… A až úspěšně v myslích mužů \[a žen\] vzbudíme náladu vzpoury proti vyhnutelným a nespravedlivým zlům, jež nás dnes ve společnosti sužují, a až lidi dovedeme k pochopení, jak vznikají a jak záleží na člověku, abychom se jich zbavili,“* pak se budeme moci sjednotit a změnit je k lepšímu. \[**Errico Malatesta: His Life and Ideas**, s. 185–186\]

Takže musíme vysvětlit, **proč** chceme systém změnit. Z této diskuze jasně vyplyne, proč anarchisté s velmi omezeným množstvím svobody v moderní společnosti nejsou spokojeni a proč chtějí vytvořit společnost skutečně svobodnou. Jak vyjádřil Noam Chomsky, anarchistická kritika moderní společnosti znamená:

> *„vyhledávat a identifikovat v každém aspektu života struktury autority, hierarchie a nadvlády a zpochybňovat je; pokud není možné pro ně podat ospravedlnění, jsou nelegitimní a patří je rozložit, aby se rozšířil rámec lidské svobody. To zahrnuje politickou moc, vlastnictví a vedení \[management\], vztahy mezi muži a ženami, rodiči a dětmi, naši moc nad osudem budoucích generací (základní morální imperativ v pozadí environmentálního hnutí…) a mnoho dalšího. Což přirozeně přináší zpochybnění obrovských institucí nátlaku a ovládání: státu, nikomu se nezodpovídajících soukromých tyranií, jež ovládají většinu domácí i mezinárodní ekonomiky \[t.j. kapitalistické korporace a firmy\], a tak dále. Ale nejen těchto.“* \[**Marxism, Anarchism, and Alternative Futures**, s. 775\]

Úloha je to tím snazší, že *„dominantní třídě“* se **nepovedlo** *„redukovat všechny své subjekty na pasivní a nevědomé nástroje svých zájmů.“* To znamená, že kde je útlak a vykořisťování, tam je i odpor – a naděje. I tehdy, když lidé utiskovaní hierarchickými sociálními vztahy své postavení obecně přijímají, instituce nedovedou uhasit jiskru naděje zcela. Dokonce samotnou svou činností pomáhají vytvářet duch vzpoury, když si lidé konečně řeknou, že čeho je moc, toho je příliš, a postaví se za svá práva. Takto v sobě hierarchické společnosti *„obsahují organické rozpory, a \[ty\] jsou jako choroboplodné zárodky smrti,“* z nichž pramení *„možnost pokroku.“* \[Malatesta, **tamtéž**, s. 186–187\]

Anarchisté proto svou kritiku stávající společnosti spojují s aktivní spoluúčastí na probíhajících zápasech, které existují v každém hierarchickém zápase. Jak rozebíráme v [sekci J](10_secJ.md) \[35\], vybízíme, aby lidé bojovali s útlakem pomocí **_přímé akce_**. Taková úsilí mění ty, kteří se jich účastní, rozbíjejí v nich společenské podmiňování, jež udržuje hierarchickou společnost v chodu, a uvědomují lidi, že jsou možné i jiné světy a že nemusíme žít jako dosud. Zápas je tedy praktická škola anarchismu, prostředek, jímž se vytvářejí nezbytné podmínky anarchistické společnosti. Anarchisté se snaží z takových úsilí učit a zároveň v nich přitom šíří své myšlenky a povzbuzují, aby se rozvinula do obecného zápasu za sociální osvobození a změnu.

Přirozený odpor utlačovaných vůči útlaku tedy přispívá k tomuto procesu ospravedlňování, k němuž Chomsky (a anarchismus) vyzývá, k tomuto kritické zhodnocení autority a dominance, podkopávání toho, co se dříve považovalo za „přirozené“ či „vycházející ze zdravého rozumu“, **než jsme to začali zpochybňovat**. Jak jsme vyjádřili výše, klíčovou součástí tohoto procesu je povzbuzovat k přímé akci utlačovaných proti jejich utlačovatelům a také podporovat anarchistické tendence a uvědomění, které existují (do větší či menší míry) v každé hierarchické společnosti. Úlohou anarchistů je podporovat taková úsilí a z nich pramenící zpochybňování společnosti a způsobu jejího fungování. Jde nám o to vybízet lidi, aby hledali prvotní příčiny sociálních problémů, s nimiž bojují, a aby usilovali o změnu základních sociálních institucí a vztahů, z nichž tyto vyrůstají. Snažíme se vytvořit uvědomění, že proti útlaku se dá nejen bojovat, ale je možné jej i ukončit, a že zápas proti nespravedlivému systému plodí zárodky společnosti, která jej nahradí. Jinými slovy je naším cílem budit naději a pozitivní vidinu lepšího světa.

Tato sekce Často kladených dotazů se nicméně přímo zabývá kritickou či „negativní“ stránkou anarchismu, odkrýváním zla vlastního vší autoritě, ať už státní, vlastnické nebo jakékoliv jiné, a následně důvody, proč anarchisté usilují o *„zničení moci, vlastnictví, hierarchie a vykořisťování.“* \[Murray Bookchin, **Post-Scarcity Anarchism**, s. 11\] Pozdější sekce naznačí, jak po analyzování světa mají anarchisté v plánu jej konstruktivně měnit, ale něco z konstruktivního jádra anarchismu můžete zhlédnout i v této sekci. Po široké kritice současného systému se přesuneme ke konkrétnějším oblastem. [Sekce C](03_secC.md) \[36\] vysvětluje anarchistickou kritiku ekonomie kapitalismu. [Sekce D](04_secD.md) \[37\] pojednává, jaký dopad mají sociální vztahy a instituce v této sekci popsané na společnost jako celek. [Sekce E](05_secE.md) \[38\] rozebírá příčiny (a některá navrhovaná řešení) ekologických problémů, jimž čelíme.


## B.1 Proč anarchisté odmítají autoritu a hierarchii? ##


### B.1.1 Jaké dopady mají autoritářské sociální vztahy? ###


### B.1.2 Je kapitalismus hierarchický? ###


### B.1.3 Jakou hierarchii hodnot kapitalismus vytváří? ###


### B.1.4 Proč existují rasismus, sexismus a homofobie? ###


### B.1.5 Jak vzniká masově-psychologický základ autoritářské civilizace? ###


### B.1.6 Dá se hierarchie ukončit? ###


## B.2 Proč anarchisté odmítají stát? ##


### B.2.1 Jaká je hlavní funkce státu? ###


### B.2.2 Má stát i druhotné funkce? ###


### B.2.3 Jak si vládnoucí třída udržuje moc nad státem? ###


### B.2.4 Jak státní centralizace ovlivňuje svobodu? ###


### B.2.5 Komu centralizace prospívá? ###


### B.2.6 Může být ve společnosti stát nezávislou silou? ###


## B.3 Proč anarchisté odmítají soukromé vlastnictví? ##


### B.3.1 Jaký je rozdíl mezi soukromým vlastnictvím a majetkem? ###


### B.3.2 Které druhy soukromého vlastnictví stát ochraňuje? ###


### B.3.3 Proč je soukromé vlastnictví vykořisťovatelské? ###


### B.3.4 Dá se soukromí vlastnictví ospravedlnit? ###


### B.3.5 Liší se od soukromého vlastnictví státní vlastnictví? ###


## B.4 Jak ovlivňuje kapitalismus svobodu? ##


### B.4.1 Zakládá se kapitalismus na svobodě? ###


### B.4.2 Zakládá se kapitalismus na sebe-vlastnění? ###


### B.4.3 Nikdo tě přece nenutí, abys pro ně pracoval! ###


### B.4.4 Ale co s obdobími velké poptávky po pracovní síle? ###


### B.4.5 Ale já chci, aby mě „nechali na pokoji“!


## B.5 Dává kapitalismus jedincům moc? Je založen na lidském činu? ##


## B.6 Ale nebudou s nejlepšími rozhodnutími přicházet jedinci nakládající s vlastními penězi? ##


## B.7 Jaké existují v moderní společnosti třídy? ##


### B.7.1 A existují vůbec třídy? ###


### B.7.2 Vyrovnává se třídní nerovnost sociální mobilitou? ###


### B.7.3 Proč se zapírá existence tříd? ###


### B.7.4 Co mají anarchisté na mysli „třídním vědomím“? ###

