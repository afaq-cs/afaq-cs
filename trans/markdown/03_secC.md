# Sekce C – Jaké jsou mýty kapitalistické ekonomie? #


## Úvod ##

V kapitalismu hraje ekonomie důležitou ideologickou roli. Posloužila už k vystavění teorie, z níž jsou už z definice vyloučeny vykořisťování a útlak. Pokusíme se zde vysvětlit, proč je kapitalismus hluboce vykořisťující. Na jiném místě, v [sekci B](02_secB.md), jsme ukázali, proč je kapitalismus utiskující, a nebudeme se zde opakovat.

## C.1 Co je s ekonomií špatně? ##


### C.1.1 Skutečně ekonomie postrádá hodnoty? ###


### C.1.2 Je ekonomie věda? ###


### C.1.3 Může se ekonomie zakládat na individualismu? ###


### C.1.4 Co je špatně s analýzou rovnovážného stavu? ###


### C.1.5 Skutečně ekonomie odráží realitu kapitalismu? ###


### C.1.6 Je možná kapitalistická ekonomie nezakládající se na rovnovážném stavu? ###


## C.2 Proč je kapitalismus vykořisťující? ##


### C.2.1 Co je to „nadhodnota“? ###


### C.2.2 Jak se vykořisťování odehrává? ###


### C.2.3 Stačí vlastnictví kapitálu k ospravedlnění zisku? ###


### C.2.4 Reprezentují zisky produktivitu kapitalismu? ###


### C.2.5 Reprezentují zisky příspěvek kapitálu k produkci? ###


### C.2.6 Ospravedlňuje „časová hodnota“ peněz úroky? ###


### C.2.7 Nejsou úroky a zisky odměnou za čekání? ###


### C.2.8 Jsou zisky výsledkem inovace a podnikatelské činnosti? ###


### C.2.9 Odrážejí zisky odměnu za riziko? ###


## C.3 Čím je určeno rozdělení mezi prací a kapitálem? ###


## C.4 Proč vládu nad trhem přebírá „Velký byznys“? ##


### C.4.1 Jak je „Velký byznys“ rozsáhlý? ###


### C.4.2 Jaké účinky má „Velký byznys“ na společnost? ###


### C.4.3 Co existence „Velkého byznysu“ znamená pro ekonomickou teorii a námezdní práci? ###


## C.5 Proč dostává „Velký byznys“ větší kus zisků? ##


### C.5.1 Nevycházejí super-zisky „Velkého byznysu“ z jeho vyšší efektivity? ###


## C.6 Může se vláda „Velkého byznysu“ na trhu změnit? ##


## C.7 Co zapříčiňuje kapitalistický hospodářský cyklus? ##


### C.7.1 Jakou roli hraje v hospodářském cyklu třídní boj? ###


### C.7.2 Jakou roli hraje v hospodářském cyklu volný trh? ###


### C.7.3 Co ještě má na hospodářský cyklus vliv? ###


## C.8 Je hospodářský cyklus důsledkem toho, že stát ovládá peníze? ##


### C.8.1 Znamená to, že keynesiánství funguje? ###


### C.8.2 Co se s keynesiánstvím stalo v 70. letech? ###


### C.8.3 Jak se kapitalismus krizi v keynesiánství přizpůsobil? ###


## C.9 Snížila by volnotržní opatření nezaměstnanost, jak zastánci kapitalismu „svobodného trhu“ tvrdí? ##


### C.9.1 Snížilo by nezaměstnanost omezení platů? ###


### C.9.2 Je nezaměstnanost důsledek příliš vysokých platů? ###


### C.9.3 Jsou odpovědí na nezaměstnanost „flexibilní“ trhy práce? ###


### C.9.4 Je nezaměstnanost dobrovolná? ###


## C.10 Je kapitalismus „svobodného trhu“ nejlepší způsob, jak snížit míru chudoby? ##


### C.10.1 Neprospívá chudým lidem ve světě neo-liberalismus? ###


### C.10.2 Prospívá „svobodný obchod“ všem? ###


### C.10.3 Prospívá „volnotržní“ kapitalismus všem, zejména lidem z pracující třídy? ###


### C.10.4 Znamená růst automaticky, že jsou na tom lidé lépe? ###


## C.11 Neprokazuje neo-liberalismus v Chile, že volný trh prospívá všem? ##


### C.11.1 Komu chilský experiment prospěl? ###


### C.11.2 A co ekonomický růst a nízká inflace v Chile? ###


### C.11.3 Potvrdilo neo-liberální Chile platnost kapitalistické ekonomie? ###


## C.12 Neukazuje se na Hong Kongu potenciál „volnotržního“ kapitalismu? ##

