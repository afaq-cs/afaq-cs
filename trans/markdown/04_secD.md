# Sekce D – Jak etatismus a kapitalismus ovlivňují společnost? #


## Úvod ##


## D.1 Proč se děje státní intervence? ##


### D.1.1 Nepocházejí problémy v první řadě právě ze státní intervence? ###


### D.1.2 Je státní intervence výsledek demokracie? ###


### D.1.3 Je státní intervence socialistická? ###


### D.1.4 Je volnotržní kapitalismus skutečně prostý státní intervence? ###


### D.1.5 Podporují státní intervenci anarchisté? ###


## D.2 Jaký vliv má bohatství na politiku? ##


### D.2.1 Je „odliv kapitálu“ opravdu tak mocný? ###


### D.2.2 Jak rozlehlá je propaganda byznysu? ###


## D.3 Jak ovlivňuje bohatství masová média? ##


### D.3.1 Jak působí struktura médií na jejich obsah? ###


### D.3.2 Jaký účinek má na masová média reklama? ###


### D.3.3 Proč média čerpají informace pouze od vlády a byznysových „expertů“? ###


### D.3.4 Jak slouží k ukázňování médií „palba kritiky“? ###


### D.3.5 Proč se jako ovládací mechanismus používá „antikomunismus“? ###


### D.3.6 Není „propagandistický model“ jen konspirační teorie? ###


### D.3.7 Neprotiřečí modelu, když média informují o selháních vlády a byznysu? ###


## D.4 Jaký je vztah mezi kapitalismem a ekologickou krizí? ##


## D.5 Co zapříčiňuje imperialismus? ##


### D.5.1 Změnil se imperialismus v průběhu času? ###


### D.5.2 Je imperialismus prostě výsledek soukromého kapitalismu? ###


### D.5.3 Znamená globalizace konec imperialismu? ###


### D.5.4 Jaký je v kapitalismu vztah mezi imperialismem a sociálními třídami? ###


## D.6 Staví se anarchisté proti nacionalismu? ##


## D.7 Odporují anarchisté národně-osvobozeneckým úsilím? ##


## D.8 Z čeho pramení militarismus a jaké má důsledky? ##


## D.9 Proč se v kapitalismu politická moc koncentruje? ##


### D.9.1 Jaký je vztah mezi polarizací bohatství a autoritářskou vládou? ###


### D.9.2 Proč se vzmáhá vládní sledování občanů? ###


### D.9.3 Vlivem čeho se objevují ospravedlnění rasismu? ###


## D.10 Jaký vliv má kapitalismus na technologii? ##


## D.11 Dá se od sebe oddělit politika a ekonomie? ##


### D.11.1 Co nám Chile říká o pravici a její představě svobody? ###


### D.11.2 Ale nedokazuje nám přece Chile, že „ekonomická svoboda“ plodí i politickou svobodu? ###

