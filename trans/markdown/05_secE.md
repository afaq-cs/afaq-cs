# Sekce E – Co je podle anarchistů příčinou ekologických problémů? #


## Úvod ##


## E.1 Jaké jsou základní příčiny našich ekologických problémů? ##


### E.1.1 Je příčinou environmentálních problémů průmysl? ###


### E.1.2 Jaký je rozdíl mezi environmentalismem a ekologií? ###


## E.2 Jakou náhradu kapitalismu navrhují eko-anarchisté? ##


## E.3 Mohou práva na soukromý majetek chránit životní prostředí? ##


### E.3.1 Zachrání přírodu její privatizace? ###


### E.3.2 Jak přispívá ekonomická moc k ekologické krizi? ###


### E.3.3 Může vyřešit ekologickou krizi zaměřenost kapitalismu na krátkodobé zisky? ###


## E.4 Dokáže volnotržní kapitalismus chránit životní prostředí? ##


### E.4.1 Skoncuje volnotržní kapitalismus se znečištěním? ###


### E.4.2 Může za volnotržního kapitalismu přežít divočina? ###


## E.5 Může ekologickou krizi zastavit etický konzum? ##


## E.6 Co je to populační mýtus? ##

