# Sekce F – Je „anarcho“-kapitalismus také druh anarchismu? #


## Úvod ##


## F.1 Jsou „anarcho“-kapitalisté skutečně anarchisté? ##


## F.2 Co mají „anarcho“-kapitalisté na mysli svobodou? ##


### F.2.1 Jak svobodu ovlivňuje soukromé vlastnictví? ###


### F.2.2 Podporují „libertariánští“ kapitalisté otroctví? ###


## F.3 Proč obvykle „anarcho“-kapitalisté nepřikládají žádnou hodnotu rovnosti? ##


### F.3.1 Proč je tato lhostejnost vůči rovnosti důležitá? ###


### F.3.2 Může v nerovné společnosti nastat soulad zájmů? ###


## F.4 Jaký postoj mají pravicoví „libertariáni“ k soukromému vlastnictví? ##


### F.4.1 Co je špatně na „osadnické“ teorii vlastnictví? ###


## F.5 Zvýší se svoboda privatizací volných statků? ##


## F.6 Je „anarcho“-kapitalismus proti státu? ##


### F.6.1 Co je špatně na této spravedlnosti „volného trhu“? ###


### F.6.2 Jaké má takový systém společenské důsledky? ###


### F.6.3 Vždyť ale Síly trhu bohatým dozajista ve zneužívání zabrání? ###


### F.6.4 Proč jsou tyto „obranné spolky“ státy? ###


## F.7 V čem dějiny „anarcho“-kapitalismu ukazují, že není anarchistický? ##


### F.7.1 Je soutěžení vlád také anarchismus? ###


### F.7.2 Je s anarchismem státní správa slučitelná? ###


### F.7.3 Může existovat „pravicový“ anarchismus? ###


## F.8 Jakou roli sehrál stát ve vytváření kapitalismu? ##


### F.8.1 Které společenské síly stojí v pozadí vzestupu kapitalismu? ###


### F.8.2 Jaký sociální kontext mělo prohlášení „laissez-faire“? ###


### F.8.3 Jaké další formy zaujala při vytváření kapitalismu státní intervence? ###


### F.8.4 Nejsou „ohrazení“ socialistický mýtus? ###


### F.8.5 A co fakt, že na americkém kontinentu ohrazení nejsou? ###


### F.8.6 Jak vnímali pracující lidé nástup kapitalismu? ###

