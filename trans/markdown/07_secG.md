# Sekce G – Je individualistický anarchismus kapitalistický? #


## Úvod ##


## G.1 Jsou individualističtí anarchisté antikapitalističtí? ##


### G.1.1 Co jejich podpora volného trhu? ###


### G.1.2 Co jejich podpora „soukromého vlastnictví“? ###


### G.1.3 Co jejich podpora námezdní práce? ###


### G.1.4 Proč je k hodnocení individualistického anarchismu důležitý sociální kontext? ###


## G.2 Proč individualističtí anarchisté zavrhují sociální anarchismus? ##


### G.2.1 Je komunistický anarchismus povinný? ###


### G.2.2 Je komunistický anarchismus násilný? ###


### G.2.3 Snaží se komunistický anarchismus zničit individualitu? ###


### G.2.4 Jaké další důvody proti komunistickému anarchismu individualisté uvádějí? ###


### G.2.5 Souhlasí většina anarchistů s individualisty, nebo s komunistickými anarchisty? ###


## G.3 Je „anarcho“-kapitalismus nová forma individualistického anarchismu? ##


### G.3.1 Je „anarcho“-kapitalismus americký anarchismus? ###


### G.3.2 Jaké jsou mezi „anarcho“-kapitalismem a individualistickým anarchismem rozdíly? ###


### G.3.3 Co „anarcho“-kapitalistická podpora „obranných sdružení“? ###


### G.3.4 Proč je důležité, že individualističtí anarchisté podporují rovnost? ###


### G.3.5 Přistoupili by individualističtí anarchisté na „rakouskou“ ekonomii? ###


### G.3.6 Vedlo by vzájemné bankovnictví zkrátka jen inflaci? ###


## G.4 Proč sociální anarchisté odmítají individualistický anarchismus? ##


### G.4.1 Je s anarchistickými principy slučitelná námezdní práce? ###


### G.4.2 Proč si sociální anarchisté myslí, že je individualismus nekonzistentní anarchismus? ###


## G.5 Benjamin Tucker: kapitalista, nebo anarchista? ##


## G.6 Jaké jsou myšlenky Maxe Stirnera? ##


## G.7 Lysander Spooner: pravicový „libertarián“, nebo libertariánský socialista? ##

