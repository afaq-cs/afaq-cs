# Sekce H – Proč anarchisté odmítají státní socialismus? #


## H.0 Úvod ##


## H.1 Odporovali anarchisté státnímu socialismu vždy? ##


### H.1.1 V čem spočívala Bakuninova kritika Marxe? ###


### H.1.2 Jaké jsou klíčové rozdíly mezi anarchisty a marxisty? ###


### H.1.3 Proč si anarchisté přejí zrušit stát „přes noc“? ###


### H.1.4 Nemají anarchisté „vůbec žádné tušení“, čím stát nahradit? ###


### H.1.5 Proč anarchisté zavrhují myšlenku „využít stávající stát“? ###


### H.1.6 Proč se anarchisté snaží „vybudovat nový svět ve schránce starého“? ###


### H.1.7 Nečetli jste Leninův „Stát a revoluci“? ###


## H.2 Které části anarchismu marxisté obzvlášť překrucují? ##


### H.2.1 Odmítají anarchisté bránit revoluci? ###


### H.2.2 Zamítají anarchisté „třídní konflikt“ a „kolektivní úsilí“? ###


### H.2.3 Prahne anarchismus „po věcech minulých“? ###


### H.2.4 Mají anarchisté za to, že „stát je úhlavní nepřítel“? ###


### H.2.5 Myslí si anarchisté, že bude „plně rozvinutý“ socialismus vytvořen přes noc? ###


### H.2.6 Jak marxisté překrucují anarchistické myšlenky o vzájemné pomoci? ###


### H.2.7 Koho anarchisté vidí jako své „činitele společenské změny“? ###


### H.2.8 Jak se má anarchismus k syndikalismu? ###


### H.2.9 Zastávají anarchisté „liberální“ politické principy? ###


### H.2.10 Jsou anarchisté proti vůdcovství? ###


### H.2.11 Jsou anarchisté „protidemokratičtí“? ###


### H.2.12 Přežívá anarchismus jen za nepřítomnosti silného hnutí pracujících? ###


### H.2.13 Odmítají anarchisté „politická“ úsilí a akce? ###


### H.2.14 Jsou anarchistické organizace „neefektivní“, „elitářské“ nebo „vyloženě bizarní“? ###


## H.3 Co jsou mýty státního socialismu? ##


### H.3.1 Chtějí anarchisté a marxisté totéž? ###


### H.3.2 Je marxismus „socialismus zdola“? ###


### H.3.3 Je leninismus „socialismus zdola“? ###


### H.3.4 Necitují anarchisté marxisty jen selektivně? ###


### H.3.5 Pozměnili marxisté anarchistické myšlenky, když si je přivlastnili? ###


### H.3.6 Je marxismus jediná revoluční politika, která zatím fungovala? ###


### H.3.7 Co je špatně na marxistické teorii státu? ###


### H.3.8 Co je špatně na leninistické teorii státu? ###


### H.3.9 Je stát prostě jen hybnou silou ekonomické moci? ###


### H.3.10 Podporoval marxismus vždy myšlenku rady pracujících? ###


### H.3.11 Má marxismus za cíl dát moc organizacím pracujících? ###


### H.3.12 Je velký byznys nezbytnou podmínkou socialismu? ###


### H.3.13 Proč je státní socialismus ve skutečnosti jen státní kapitalismus? ###


### H.3.14 Neuznávají marxisté, že by pracující měli mít moc? ###


## H.4 Nevyvrátil Engels anarchismus článkem „O autoritě“? ##


### H.4.1 Znamená organizace konec svobody? ###


### H.4.2 Ukazuje slabinu Engelsova argumentu volná láska?


### H.4.3 Jaký způsob vedení továrny anarchisté navrhují? ###


### H.4.4 Jak jsou Engelsovy argumenty vyvráceny třídním bojem? ###


### H.4.5 Operuje průmysl „nezávisle na veškeré sociální organizaci“? ###


### H.4.6 Proč Engelsův článek „O autoritě“ marxismu ubližuje? ###


### H.4.7 Je revoluce „nejautoritářštější věc ze všech“? ###


## H.5 Co je to avantgardismus a proč jej anarchisté odmítají? ##


### H.5.1 Proč jsou avantgardistické strany protisocialistické? ###


### H.5.2 Byly avantgardistické předpoklady potvrzeny? ###


### H.5.3 Proč znamená avantgardismus nutně stranickou moc? ###


### H.5.4 Opustil Lenin avantgardismus? ###


### H.5.5 Co je to „demokratický centralismus“? ###


### H.5.6 Proč anarchisté odmítají „demokratický centralismus“? ###


### H.5.7 Je důležitý způsob, jak se revolucionáři organizují? ###


### H.5.8 Jsou avantgardní strany účinné? ###


### H.5.9 V čem jsou avantgardní strany účinné? ###


### H.5.10 Proč „demokratický centralismus“ produkuje „byrokratický centralismus“? ###


### H.5.11 Můžete uvést příklady negativní povahy avantgardních stran? ###


### H.5.12 Ale nedokazuje přece Ruská revoluce, že avantgardní strany fungují? ###


## H.6 Proč Ruská revoluce selhala? ##


### H.6.1 Mohou selhání Ruské revoluce vysvětlit objektivní faktory? ###


### H.6.2 Ovlivnila bolševická ideologie výsledek Ruské revoluce? ###


### H.6.3 Byli ruští pracující „deklasovaní“ a „atomizovaní“? ###

