# Sekce I – Jak by anarchistická společnost vypadala? #


## I.0 Úvod ##


## I.1 Není libertariánský socialismus protimluv? ##


### I.1.1 Je socialismus neuskutečnitelný? ###


### I.1.2 Je libertariánský komunismus neuskutečnitelný? ###


### I.1.3 A co je vlastně špatně s trhy? ###


### I.1.4 Je-li kapitalismus vykořisťovatelský, není takový i socialismus? ###


### I.1.5 Přiděluje kapitalismus zdroje efektivně? ###


## I.2 Je toto plán anarchistické společnosti? ##


### I.2.1 Proč vůbec rozebírat, jak by anarchistická společnost vypadala? ###


### I.2.2 Bude možné z kapitalismu přejít rovnou do anarchistické společnosti? ###


### I.2.3 Jak se vytváří rámec anarchistické společnosti? ###


## I.3 Jak by mohla vypadat ekonomická struktura anarchistické společnosti? ##


### I.3.1 Co je to „syndikát“? ###


### I.3.2 Co je to samospráva pracujících? ###


### I.3.3 Co znamená socializace? ###


### I.3.4 Jaké poměry existují mezi jednotlivými syndikáty? ###


### I.3.5 Co by konfederace syndikátů dělaly? ###


### I.3.6 A co soutěž mezi syndikáty? ###


### I.3.7 A co lidé, kteří se do syndikátu přidat nechtějí? ###


### I.3.8 Zaměřují se anarchisté na „malé autonomní komunity věnující se produkci na malé šále“? ###


## I.4 Jak by fungovala anarchistická ekonomika? ##


### I.4.1 K čemu je vůbec v anarchii ekonomická aktivita? ###


### I.4.2 Proč si anarchisté přejí zrušit práci? ###


### I.4.3 Jak mají anarchisté v úmyslu zrušit práci? ###


### I.4.4 Jaká by se dala v anarchii použít ekonomická rozhodovací kritéria? ###


### I.4.5 A co „nabídka a poptávka“? ###


### I.4.6 Ale nevedl by přece komunistický anarchismus k poptávce přesahující nabídku? ###


### I.4.7 Co zajistí, aby výrobci neignorovali spotřebitele? ###


### I.4.8 A co investiční rozhodnutí? ###


### I.4.9 Mělo by se na technologický pokrok nahlížet jako na protianarchistický? ###


### I.4.10 Jakou výhodu by měla široká báze přerozdělování nadhodnoty? ###


### I.4.11 Pokud socialismus eliminuje pohnutku zisku, neublíží to výkonu? ###


### I.4.12 Nabude mít kapitalismus tendenci se znovu objevovat? ###


### I.4.13 Kdo bude odvádět špinavou či nepříjemnou práci? ###


### I.4.14 A co člověk, který nebude pracovat? ###


### I.4.15 Jak bude vypadat pracoviště zítřku? ###


### I.4.16 Nebude libertariánská komunistická společnost neefektivní? ###


## I.5 Jak by vypadala sociální struktura anarchie? ##


### I.5.1 Co jsou to participativní komunity? ###


### I.5.2 Proč jsou konfederace participativních komunit potřebné? ###


### I.5.3 Produkovaly by konfederace byrokraty a politiky? ###


### I.5.4 Jak se vůbec cokoliv na všech těch schůzích dohodne? ###


### I.5.5 Nejsou participativní komunity a konfederace prostě nové státy? ###


### I.5.6 Nevyvstávalo by v anarchii nebezpečí „tyranie většiny“? ###


### I.5.7 A co když se do komuny nechci přidat? ###


### I.5.8 Co zločin? ###


### I.5.9 Jak je na tom v anarchii svoboda projevu? ###


### I.5.10 Co politické strany, zájmové skupiny a profesní seskupení? ###


### I.5.11 Jak se bude anarchie bránit před mocichtivými? ###


### I.5.12 Poskytovala by anarchistická společnost zdravotní péči a další veřejné služby? ###


## I.6 A co „Tragédie volných statků“?


### I.6.1 Jak se dá využívat majetek „vlastněný všemi na světě“? ###


### I.6.2 Neomezuje komunitní vlastnictví individuální svobodu? ###


## I.7 Nezničí libertariánský socialismus individualitu? ##


### I.7.1 Naznačují kmenové kultury, že komunalismus brání individualitu? ###


### I.7.2 Uctívají anarchisté minulost nebo „ušlechtilého divocha“? ###


### I.7.3 Je k ochraně individuálních práv nutný zákon? ###


### I.7.4 Ochraňuje kapitalismus individualitu? ###


## I.8 Ukazuje se na revolučním Španělsku, že může libertariánský socialismus v praxi fungovat? ##


### I.8.1 Je Španělská revoluce jako model pro moderní společnosti neaplikovatelná? ###


### I.8.2 Jak ve Španělsku zvládli anarchisté získat hromadnou lidovou podporu? ###


### I.8.3 Jak byla organizována španělská průmyslová družstva? ###


### I.8.4 Jak byla španělská průmyslová družstva koordinována? ###


### I.8.5 Jak byla organizována a koordinována španělská zemědělská družstva? ###


### I.8.6 Čeho zemědělská družstva dosáhla? ###


### I.8.7 Vznikala vesnická družstva za použití síly? ###


### I.8.8 Ale děla se ve španělských družstvech inovace? ###


### I.8.9 Proč to nepřežilo, když to bylo tak dobré? ###


### I.8.10 Proč CNT kolaborovala se státem? ###


### I.8.11 Vycházelo rozhodnutí ke kolaboraci z anarchistické teorie? ###


### I.8.12 Bylo rozhodnutí ke kolaboraci vnuceno členstvu CNT? ###


### I.8.13 Jaká vzešla z revoluce politická ponaučení? ###


### I.8.14 Jaká vzešla z revoluce ekonomická ponaučení? ###

