# Sekce J – Co anarchisté dělají? #


## J.0 Úvod ##


## J.1 Zapojují se anarchisté do sociálních bojů? ##


### J.1.1 Proč jsou sociální boje důležité? ###


### J.1.2 Jsou anarchisté proti reformám? ###


### J.1.3 Proč jsou anarchisté proti reformismu? ###


### J.1.4 Jaký postoj zaujímají anarchisté vůči kampaním „jednoho tématu“? ###


### J.1.5 Pro se snaží anarchisté sociální boje zobecňovat? ###


## J.2 Co je to přímá akce? ##


### J.2.1 Proč anarchisté za účelem změny upřednostňují přímou akci? ###


### J.2.2 Proč anarchisté jako nástroj změny odmítají účast ve volbách? ###


### J.2.3 Jaké má účast ve volbách politické implikace? ###


### J.2.4 Ale nebude přece účinné volit radikální strany? ###


### J.2.5 Proč podporují anarchisté abstencionismus a jaké jsou jeho implikace? ###


### J.2.6 K jakým důsledkům vede, když radikálové používají volební agitaci? ###


### J.2.7 A neměli bychom volit reformistické strany, abychom odkryli jejich povahu? ###


### J.2.8 Povede abstencionismus k volebnímu vítězství pravice? ###


### J.2.9 Co dělají anarchisté místo voleb? ###


### J.2.10 Znamená zavrhnutí volební agitace, že jsou anarchisté apolitičtí? ###


## J.3 Jaké organizace anarchisté budují? ##


### J.3.1 Co jsou to afinitní skupiny? ###


### J.3.2 Co jsou to „syntetické“ federace? ###


### J.3.3 Co je to „Platforma“? ###


### J.3.4 Proč mnoho anarchistů „Platformu“ odmítá? ###


### J.3.5 Jsou i další druhy anarchistických federací? ###


### J.3.6 Jakou úlohu hrají tyto skupiny v anarchistické teorii? ###


### J.3.7 Nedokazuje Bakuninův pojem „Neviditelná diktatura“, že jsou anarchisté vskrytu autoritáři? ###


### J.3.8 Co je to anarchosyndikalismus? ###


### J.3.9 Proč mnoho anarchistů nepatří mezi anarchosyndikalisty? ###


## J.4 Jaké tendence ve společnosti napomáhají anarchistické činnosti? ##


### J.4.1 Proč je sociální boj dobré znamení? ###


### J.4.2 Nepřinese sociální boj více škod než užitku? ###


### J.4.3 Vnímají anarchisté nová sociální hnutí jako pozitivní vývoj? ###


### J.4.4 Co je to „ekonomická strukturální krize“? ###


### J.4.5 Proč je „ekonomická strukturální krize“ pro sociální boj důležitá? ###


### J.4.6 Jaké implikace mají nálady proti vládě a velkému byznysu? ###


### J.4.7 A co komunikační revoluce? ###


## J.5 Jaké alternativní sociální organizace anarchisté vytvářejí? ##


### J.5.1 Co je to komunitní odborářství? ###


### J.5.2 Proč anarchisté podporují průmyslové odborářství? ###


### J.5.3 Jaký postoj zastávají anarchisté ke stávajícím odborům? ###


### J.5.4 Co jsou to průmyslové sítě? ###


### J.5.5 Které formy družstevních záložen anarchisté podporují? ###


### J.5.6 Proč jsou vzájemné záložní programy důležité? ###


### J.5.7 Má většina anarchistů za to, že vzájemné spoření stačí k odstranění kapitalismu? ###


### J.5.8 Jak by vypadal moderní systém vzájemného bankovnictví? ###


### J.5.9 Jak vzájemné spoření funguje? ###


### J.5.10 Proč anarchisté podporují družstva? ###


### J.5.11 Pokud skutečně pracující chtějí samosprávu, proč je tak málo družstev? ###


### J.5.12 Kdyby byla samospráva efektivnější, nezavedli by ji přece i kapitalisté? ###


### J.5.13 Co jsou to Moderní školy? ###


### J.5.14 Co je to libertariánský municipalismus? ###


### J.5.15 Jaký postoj zaujímají anarchisté k sociálnímu státu? ###


### J.5.16 Nabízí historie příklady kolektivní svépomoci? ###


## J.6 Jaké metody výchovy dětí anarchisté obhajují? ##


### J.6.1 Které hlavní překážky stojí v cestě výchově svobodných dětí? ###


### J.6.2 Jaké jsou příklady libertariánských metod výchovy dětí? ###


### J.6.3 Jak můžou děti, které se nemají čeho bát, být ukázněné? ###


### J.6.4 Není „libertariánská výchova dětí“ jen jiným výrazem pro rozmazlování? ###


### J.6.5 Jakou pozici zastávají anarchisté vůči sexuálnímu osvobození dospívajících? ###


### J.6.6 Ale neodvádí tato starost o sexuální osvobození pozornost od revoluce? ###


## J.7 Co mají anarchisté na mysli sociální revolucí? ##


### J.7.1 Proč jsou anarchisté většinou revolucionáři? ###


### J.7.2 Je sociální revoluce uskutečnitelná? ###


### J.7.3 Neznamená revoluce násilí? ###


### J.7.4 Co by sociální revoluce zahrnovala? ###


### J.7.5 Jakou roli hrají v sociální revoluci anarchisté? ###


### J.7.6 Jak by se mohla anarchistická revoluce bránit? ###

