# Příloha: Anarchismus a „anarcho“-kapitalismus #


## Odpovědi na některé omyly a překroucení v „Anarchist Theory FAQ“ verze 5.2 Bryana Caplana ##


### 1 Individualističtí anarchisté a socialistické hnutí. ###


### 2 Proč je Caplanova definice socialismu chybná? ###


### 3 Byl Proudhon socialista, nebo kapitalista? ###


### 4 Tucker o vlastnictví, komunismu a socialismu ###


### 5 Anarchismus a „anarcho“-kapitalismus. ###


### 6 Dodatek: Definování anarchismu ###


## Odpovědi na některé omyly a překroucení v „Anarchist Theory FAQ“ verze 4.1.1 Bryana Caplana ##


### 1 Je anarchismus čistě negativní? ###


### 2 Anarchismus a rovnost ###


### 3 Je anarchismus totéž co socialismus? ###


### 4 Anarchismus a disidenti ###


### 5 Jak by anarchokapitalismus fungoval? ###


## Je „anarcho“-kapitalismus druh anarchismu? ##

