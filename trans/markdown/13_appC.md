# Příloha: Anarchismus a marxismus #


## Odpověď na omyly a překroucení v pojednání Davida McNallyho „Socialism from Below“ ##


### 1. Úvod ###


### 2. Je anarchismus politika „drobných vlastníků“? ###


### 3. „Glorifikuje“ anarchismus „hodnoty z minulosti“? ###


### 4. Proč McNallyho komentáře zkreslují Proudhonovy myšlenky? ###


### 5. Proč McNallyho komentáře zkreslují Bakuninovy myšlenky?


### 6. Jsou to „osobní zvláštnosti“, nebo věci „zakořeněné v samotné podstatě anarchistické nauky“? ###


### 7. Jsou anarchisté proti demokracii? ###


### 8. Jsou leninisté pro demokracii? ###


### 9. Proč se McNally mýlí, co se týče vztahu syndikalismu k anarchismu? ###


### 10. Odmítají syndikalisté politickou akci pracující třídy? ###


### 11. Proč je chybné McNallyho tvrzení, že leninismus podporuje sebe-emancipaci pracující třídy? ###


### 12. Proč si marxistická „třídní analýza“ anarchismu protiřečí? ###


### 13. Je-li marxismus „socialismus zdola“, proč jej anarchisté odmítají? ###


### 14. Proč je nečestné, že McNally používá pojem „socialismus zdola“? ###


### 15. Udržel Trockij naživu Leninovu „demokratickou esenci“? ###


## Marxisté a španělský anarchismus ##


### 1. Byli španělští anarchisté „primitivní rebelové“? ###


### 2. Jak přesná je kniha Felixe Morrowa o Španělské revoluci? ###


### 3. Byla CNT ovládána „vysoce centralizovanou“ FAI? ###


### 4. Jaká je historie CNT a Komunistické internacionály? ###


### 5. Proč se CNT nepřidala k Dělnické alianci? ###


### 6. Byla Říjnová revolta roku 1934 sabotována CNT? ###


### 7. Byli Přátelé Durrutiho marxisté? ###


### 8. „Rozešli se“ Přátelé Durrutiho s anarchismem? ###


### 9. Byli Přátelé Durrutiho ovlivněni trockisty? ###


### 10. Co nám říká program Přátel Durrutiho o trockismu? ###


### 11. Proč je Morrowův komentář proti militarizaci Milic ironický? ###


### 12. Co je ironického na Morrowově vizi revoluce? ###


### 13. Proč anarchisté odmítají marxistický „dělnický stát“? ###


### 14. Co je špatně s Morrowovou „principiální zásadou“ anarchismu? ###


### 15. Zaměřoval se před Revolucí španělský anarchismus na vytváření „družstev“? ###


### 16. Jak vývoj družstev naznačuje rozdíly mezi bolševismem a anarchismem? ###


### 17. Proč je ironická Morrowova podpora „proletářských metod výroby“? ###


### 18. Znamenaly federace družstev „opuštění“ anarchistických myšlenek? ###


### 19. Vyvrátila anarchismus zkušenost s vesnickými družstvy? ###


### 20. Značí zkušenost ze Španělské revoluce selhání anarchismu nebo selhání anarchistů? ###


## Odpověď na omyly a překroucení v článku „Marxism and direct action“ Phila Mitchinsona ##


### … ###


## Odpověď na omyly a překroucení v textu „Marxism and Anarchism“ strany SWP ##


### … ###


## Odpověď na omyly a překroucení v textu „Why we must further Marxism and not Anarchism“ Johna Fishera ##


### … ###

