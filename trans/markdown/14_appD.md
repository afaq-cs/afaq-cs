# Příloha – Ruská revoluce #


## Co se stalo během Ruské revoluce? ##


### 1 Můžete krátce shrnout, co se stalo roku 1917? ###


### 2 Jak získali bolševici hromadnou podporu? ###


### 3 Nedokazuje ale Ruská revoluce, že avantgardní strany fungují? ###


### 4 Byl po Říjnu aplikován Leninův „Stát a revoluce“? ###


### 5 Bylo skutečně cílem bolševiků dát moc Sovětům? ###


### 6 Co se se Sověty stalo po Říjnu? ###


### 7 Jak se vyvinulo hnutí továrních komisí? ###


### 8 Jaký byl v roce 1917 postoj bolševiků k „dělnickému řízení“? ###


### 9 Co se stalo s továrními komisemi po Říjnu? ###


### 10 V čem spočívala bolševická ekonomická politika v roce 1918? ###


### 11 Fungovala bolševická ekonomická opatření? ###


### 12 Existovala k Leninovu „státnímu kapitalismu“ a „válečnému komunismu“ alternativa? ###


### 13 Povolovali bolševici nezávislé odbory? ###


### 14 Byla Rudá armáda skutečně revoluční armádou? ###


### 15 Byla Rudá armáda „naplněná socialistickým vědomím“? ###


### 16 Jak začala občanská válka a jak se vyvíjela? ###


### 17 Probíhala občanská válka jen mezi Rudými a Bílými? ###


### 18 Jak rozsáhlá byla imperialistická intervence? ###


### 19 Změnila se po skončení občanské války bolševická politika? ###


### 20 Dá se Rudý teror a Čeka ospravedlnit? ###


### 21 Fungovala bolševická politika zaměřená na rolníky? ###


### 22 Existovala k zabavování obilí alternativa? ###


### 23 Byla represe socialistické opozice oprávněná? ###


### 24 Co během revoluce dělali anarchisté? ###


### 25 Vyvrátila Ruská revoluce anarchismus? ###


## Co byla zač Kronštandtská vzpoura? ##


### 1 Proč je Kronštadtská vzpoura důležitá? ###


### 2 Jaký měla Kronštadtská vzpoura kontext? ###


### 3 V čem spočíval Kronštadtský program? ###


### 4 Odrážela Kronštadtská vzpoura „vztek rolnictva“? ###


### 5 Jaké lži bolševici o Kronštadtu šířili? ###


### 6 Byla Kronštadtská vzpoura bělogvardějským spiknutím? ###


### 7 Jaký byl skutečný poměr Kronštadtu k bělogvardějcům? ###


### 8 Zahrnovala vzpoura i nové námořníky? ###


### 9 Byl Kronštadt politicky odlišný? ###


### 10 Proč Kronštadt nepodpořili petrohradští dělníci? ###


### 11 Byli během Kronštadtské vzpoury bělogvardějci hrozbou? ###


### 12 Byla země příliš unavená na to, aby dovolila sovětskou demokracii? ###


### 13 Existovala ke kronštadtské „třetí revoluci“ skutečná alternativa? ###


### 14 Jak Kronštadt překrucují dnešní trockisté? ###


### 15 Co nám říká Kronštadt o bolševismu? ###


## Co zapříčinilo degeneraci Ruské revoluce? ##


### 1 Ignorují anarchisté objektivní faktory, jimž Ruská revoluce čelila? ###


### 2 Mohou „objektivní faktory“ opravdu selhání bolševismu vysvětlit? ###


### 3 Může selhání bolševismu vysvětlit občanská válka? ###


### 4 Zničil revoluci ekonomický kolaps a izolace? ###


### 5 Byla ruská pracující třída atomizovaná či „deklasovaná“? ###


### 6 Vinili bolševici za své činy „objektivní faktory“? ###


## Jak přispěla bolševická ideologie k selhání revoluce? ##


### 1 Jaký vliv měl marxistický historický materialismus na bolševismus? ###


### 2 Proč marxistická teorie státu podkopala moc dělnické třídy? ###


### 3 Jak ovlivnila revoluci Engelsova esej „O autoritě“? ###


### 4 Jaká byla bolševická představa demokracie? ###


### 5 Jaký důsledek měla bolševická představa „socialismu“? ###


### 6 Jak revoluci ovlivnilo, že bolševici upřednostňovali nacionalizaci? ###


### 7 Jak revoluci ovlivnilo, že bolševici upřednostňovali centralizaci? ###


### 8 V čem zaměření na stranickou moc podkopalo revoluci? ###


## Byla některá z opozic bolševikům skutečnou alternativou? ##


### 1 Byli alternativou „levicoví komunisté“ z roku 1918? ###


### 2 Jaké nedostatky měla „dělnická opozice“ z roku 1920? ###


### 3 Co Trockého „levicová opozice“ ve 20. letech? ###


### 4 Co nám tyto opozice sdělují o podstatě leninismu? ###


## V čem ukazuje machnovské hnutí, že k bolševismu existuje alternativa? ##


### 1 Kdo byl Nestor Machno? ###


### 2 Proč se hnutí pojmenovalo po Machnovi? ###


### 3 Proč se Machnovi přezdívalo „baťko“? ###


### 4 Můžete krátce shrnout přehled machnovského hnutí? ###


### 5 Jak se machnovci organizovali? ###


### 6 Měli machnovci konstruktivní sociální program? ###


### 7 Uskutečňovali své myšlenky v praxi? ###


### 8 Nebyli machnovci prostě jen kulaci? ###


### 9 Byli machnovci antisemité a pogromisté? ###


### 10 Nenáviděli machnovci města a městské dělníky? ###


### 11 Byli machnovci nacionalisté? ###


### 12 Podporovali machnovci bělogvardějce? ###


### 13 Jaký poměr měli k hnutí bolševici? ###


### 14 V čem se machnovci lišili od bolševiků? ###


### 15 Jak moderní následníci bolševismu očerňují machnovce? ###


### 16 Jaká ponaučení si můžeme z machnovců odnést? ###

